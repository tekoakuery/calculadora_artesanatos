import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# utils: https://www.miniwebtool.com/django-secret-key-generator/
SECRET_KEY = '*change-me*change-me*change-me*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
LOCAL = True

ALLOWED_HOSTS = ["*"]

INTERNAL_IPS = ['127.0.0.1', 'localhost',]

SESSION_COOKIE_NAME = 'calculadora_artesanatos.local'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'database_name',
        'USER': 'postgres',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

# Show message in terminal instead send mail. Set only in dev env
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# LOG - LOCAL
# handler: console - level: debug
# handler: file - level: warnning
# LOG - PROD
# handler: file - level: error
# handler: mail - level: error (TODO: config mail)

LOG_ROOT = os.path.join(BASE_DIR, 'logs')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'file_debug': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.FileHandler',
            'filename': f'{LOG_ROOT}/debug.log',
        },
        'file_error': {
            'level': 'ERROR' ,
            'class': 'logging.FileHandler',
            'filename': f'{LOG_ROOT}/error.log',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console',
                         'file_debug',
                         'file_error',
                         'mail_admins',
                         ],
            'propagate': True,
        },
    },
}
