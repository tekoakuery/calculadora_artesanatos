from django.db import models
from django.contrib.auth.models import AbstractUser

from preco_justo.models import TekoaCadastro


# https://www.youtube.com/watch?v=cXOZUJL-MEA
class Usuaria(AbstractUser):
    tekoa = models.ForeignKey(
        TekoaCadastro, on_delete=models.PROTECT,
        null=True, blank=True,
        related_name='usuarias',
        help_text='Caso seja uma conta de artesã/ão, selecione a Tekoa.')

    class Meta(AbstractUser.Meta):
        verbose_name='usuária'
        verbose_name_plural='usuárias'

    def __str__(self):
        last_name = self.last_name
        if self.first_name:
            if last_name:
                return f"{self.first_name} {last_name}"
            else:
                return self.first_name
        return super().__str__()

    @property
    def tekoa_recent_registration(self):
        return TekoaCadastro.objects.last()
