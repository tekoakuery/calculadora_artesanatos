from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth import admin as auth_admin

from .forms import UserChangeForm, UserCreationForm
from .models import Usuaria


@admin.register(Usuaria)
class UsuariaAdmin(auth_admin.UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    model = Usuaria
    autocomplete_fields = ['tekoa',]
    add_fieldsets = (
            ('Informações pessoais', {'fields': ('first_name', 'last_name', 'tekoa')}),
            ('Dados de autenticação', {'fields': ('username', 'password1', 'password2')}),)
    fieldsets = (
            ("Tekoa da artesã", {'fields': ("tekoa",)}),
            ) + auth_admin.UserAdmin.fieldsets
    list_filter = (
            "tekoa",
            ) + auth_admin.UserAdmin.list_filter
    list_display = ('first_name', 'last_name', 'tekoa', 'username')


# Disabled Group manage for now
admin.site.unregister(Group)
