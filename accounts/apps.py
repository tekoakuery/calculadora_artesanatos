from django.apps import AppConfig


class AccountsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'accounts'
    verbose_name='Conta de usuária'
    verbose_name_plural='Contas de usuárias'
