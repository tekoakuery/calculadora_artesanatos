#!/bin/bash

mkdir -p docs
cp -r docs_src/* docs/
cp README.rst docs/
cp docker-instructions.md docs/
cp preco_justo/README.rst docs/preco_justo_readme.rst

python manage.py graph_models preco_justo > docs/model_preco_justo.dot

# Compose Viz
# ./docker-django.sh service viz -o docs/compose_viz-project.dot
compose_addons_cfg=docker-compose-addons.cfg
docker-compose \
    $(egrep -v '^#' $compose_addons_cfg | grep \.prod | xargs) \
    config > docs/docker-compose.merged.yaml

cat docs/docker-compose.merged.yaml

# docker run --rm -it --name dcv \
#     -v $(pwd):/input pmsipilot/docker-compose-viz render \
#     -m dot \
#     -o docs/compose_viz-project.dot \
#     --horizontal \
#     --no-volumes \
#     --no-ports \
#     --no-networks \
#     --force \
#     -- docs/docker-compose.merged.yaml

render \
    -m dot \
    -o docs/compose_viz-project.dot \
    --horizontal \
    --no-volumes \
    --no-ports \
    --no-networks \
    --force \
    -- docs/docker-compose.merged.yaml

sphinx-apidoc -f -o docs . *migrations manage.py
sphinx-build -b html docs public
