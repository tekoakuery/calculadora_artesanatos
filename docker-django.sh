#!/bin/bash

container-engine() {
    if [[ $(podman --version 2> /dev/null) ]]; then
        podman "$@"
    elif [[ $(docker --version 2> /dev/null) ]]; then
        docker "$@"
    else
        echo ""
        echo "No CONTAINER ENGINE was found on your operating system."
        echo "This program requires the installation of PODMAN or DOCKER to continue."
        echo ""
        exit;
    fi
}

container-compose() {
    RUNSERVER_PORT=$(shuf -i 8000-8009 -n 1)
    if [ $1 = 'up' ]; then
        # verifica se existe o arquivo .envs/.env.dev. Caso não:
        # sai dessa consição e segue o script
        env_dev_file=.envs/.env.dev
        if [[ ! -z $(ls -la $env_dev_file 2> /dev/null) ]]; then
            # verifica se RUNSERVER_PORT=800X está definido em $env_dev_file
            # se tiver definido, sai dessa consição e segue o script
            RUNSERVER_PORT_ATTR=$(grep -R "^RUNSERVER_PORT=[0-9.:]\{4,20\}$" $env_dev_file)
            if [[ ! -z $RUNSERVER_PORT_ATTR ]]; then
                RUNSERVER_PORT=$(echo $RUNSERVER_PORT_ATTR | sed 's/[^0-9\.\:]*//g')

                if [[ $@ = 'up' ]]; then
                    echo ""
                    echo "Application running on port $RUNSERVER_PORT"
                    echo ""
                    sleep 0.5
                fi
            else
                # se não tiver definido, solicita atribuir no arquivo $env_dev_file

                # atribui RUNSERVER_PORT em .envs/.env.dev
                RUNSERVER_PORT=8000
                echo ""
                echo "You have not set the runserver port in the settings file .envs/.env.dev."
                echo "This setting will be saved in the .envs/.env.dev file as a RUNSERVER_PORT parameter and can be changed at any time."
                echo -n "Please, informe the runserver port [8000]:"
                read
                echo ""
                if [[ ! -z ${REPLY} ]]; then
                    RUNSERVER_PORT=${REPLY}
                fi

                echo "" >> $env_dev_file
                echo "####################" >> $env_dev_file
                echo "### RUNSERVER config" >> $env_dev_file
                echo "####################" >> $env_dev_file
                echo "RUNSERVER_PORT=$RUNSERVER_PORT" >> $env_dev_file
            fi
        fi
    fi

    # check volumes settings
    env_volumes_file=.envs/.env.volumes
    if [[ -z $(ls -la $env_volumes_file 2> /dev/null) ]]; then
        cp docker_django/$env_volumes_file $env_volumes_file
    fi

    # check compose addons settings
    compose_addons_cfg=docker-compose-addons.cfg
    if [[ $(podman-compose --version 2> /dev/null) ]]; then
        eval $(egrep -v '^#' $env_volumes_file | xargs) \
            DJANGO_PROJECT_NAME=calculadora_artesanatos \
            RUNSERVER_PORT=$RUNSERVER_PORT \
            PODMAN_USERNS=keep-id \
            podman-compose \
            -p calculadora_artesanatos \
            $(egrep -v '^#' $compose_addons_cfg | grep -v \.prod | xargs) \
            --no-cleanup "$@"
    elif [[ $(docker-compose --version 2> /dev/null) ]]; then
        eval $(egrep -v '^#' $env_volumes_file | xargs) \
            DJANGO_PROJECT_NAME=calculadora_artesanatos \
            RUNSERVER_PORT=$RUNSERVER_PORT \
            docker-compose \
            -p calculadora_artesanatos \
            $(egrep -v '^#' $compose_addons_cfg | grep -v \.prod | xargs) \
            "$@"
    else
        echo ""
        echo "No CONTAINER ENGINE COMPOSE was found on your operating system."
        echo "This program requires the installation of PODMAN-COMPOSE or DOCKER-COMPOSE to continue."
        echo ""
        exit;
    fi

    if [ $1 = 'up' ]; then
        echo ""
        echo "Application running on port $RUNSERVER_PORT"
        echo ""

        # get podman ip
        podman_ip="$(get_podman_ip)"
        if [[ ! -z $podman_ip ]]; then
            podman_ip_info=
            # check if podman ip exists in $env_dev_file
            INTERNAL_IPS_ATTR="$(grep -R "^INTERNAL_IPS=.*$" $env_dev_file)"
            if [[ -z $INTERNAL_IPS_ATTR ]]; then
                podman_ip_info=yes
            else
                if [[ -z $(echo $INTERNAL_IPS_ATTR | grep $podman_ip) ]]; then
                    podman_ip_info=yes
                fi
            fi

            if [[ ! -z $podman_ip_info ]]; then
                # if not exists, print info
                echo "IMPORTANT: For the proper functioning of the application, mainly for DEBUG_TOOLBAR,"
                echo "we recommend adding the podman ip in INTERNAL_IPS on Django setting."
                echo "This environment variable can be found in $env_dev_file."
                echo "podman ip addr: $podman_ip"
                echo "Note: Changes on $env_dev_file file require a restart of the container composition."
                echo ""
            fi
        fi
    fi
}

django_project_name_filter() {
    echo calculadora_artesanatos
}

get_podman_ip() {
    if [[ $(podman --version 2> /dev/null) ]]; then
        podman_ip="$(exec ip addr | grep 'inet ' | grep -v 127\.0 | awk '{print $2}')"
        echo "${podman_ip%/*}"
    fi
}

compose() {
    container-compose "$@"
}

exec-without-tty() {
    container-engine exec $(container-engine ps -q -f name=$(django_project_name_filter)_web) "$@"
}

exec() {
    container-engine exec -it $(container-engine ps -q -f name=$(django_project_name_filter)_web) "$@"
}

attach() {
    container-engine attach $(container-engine ps -q -f name=$(django_project_name_filter)_web) "$@"
}

service() {
    local cmdname=$1; shift
    if type "service_$cmdname" >/dev/null 2>&1; then
        "service_$cmdname" "$@"
    else
        command service "$cmdname" "$@"
    fi
}

service_start() {
    compose_addons_cfg=docker-compose-addons.cfg
    if [[ $(podman --version 2> /dev/null) ]]; then
        DJANGO_PROJECT_NAME=calculadora_artesanatos \
            IMAGE_APP_PRODUCTION=fabiopiovam/calculadora_artesanatos:1.0-python-gunicorn \
            PODMAN_USERNS=keep-id \
            podman-compose \
            -p calculadora_artesanatos \
            $(egrep -v '^#' $compose_addons_cfg | grep \.prod | xargs) \
            --no-cleanup up -d
    else
        DJANGO_PROJECT_NAME=calculadora_artesanatos \
            IMAGE_APP_PRODUCTION=fabiopiovam/calculadora_artesanatos:1.0-python-gunicorn \
            docker stack deploy \
            $(egrep -v '^#' $compose_addons_cfg | grep \.prod | xargs) \
            --prune \
            calculadora_artesanatos
    fi
}

service_stop() {
    compose_addons_cfg=docker-compose-addons.cfg
    if [[ $(podman --version 2> /dev/null) ]]; then
        DJANGO_PROJECT_NAME=calculadora_artesanatos \
            IMAGE_APP_PRODUCTION=fabiopiovam/calculadora_artesanatos:1.0-python-gunicorn \
            PODMAN_USERNS=keep-id \
            podman-compose \
            -p calculadora_artesanatos \
            $(egrep -v '^#' $compose_addons_cfg | grep \.prod | xargs) \
            --no-cleanup down -t 0
    else
        docker stack rm calculadora_artesanatos
    fi
}

service_restart() {
    service_stop
    service_start
}

service_viz () {
    # TODO:
    # * separate by env dev & prod (priority prod)
    # * generate .dot to documentation
    # * update doc script with viz context & instructions

    # get merged compose from podman
    # compose ps | grep "\{.*\}"

    # convert json to yaml
    # exec python -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))' <tmp.json > tmp.yaml
    python -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))' <tmp.json > tmp.yaml

    # generate viz
    # container-engine run --rm -it --name dcv \
    #     -v $(pwd):/input pmsipilot/docker-compose-viz render \
    #     -m image \
    #     -o viz/test.png \
    #     --force \
    #     "$@" \
    #     -- tmp.yaml
    container-engine run --rm -it --name dcv \
        -v $(pwd):/input pmsipilot/docker-compose-viz render \
        -m dot \
        --force \
        "$@" \
        -- tmp.yaml
}

restore() {
    local cmdname=$1; shift
    if type "restore_$cmdname" >/dev/null 2>&1; then
        "restore_$cmdname" "$@"
    else
        command restore "$cmdname" "$@"
    fi
}

restore_db() {
    env_restore_file='.envs/.env.aws.restore'
    if [ ! -z $(ls $env_restore_file 2>/dev/null) ]; then
        source $env_restore_file
    else
        echo ""
        BACKUP_S3_BUCKET='bucket-name'
        echo -n "Type bucket S3 name of backup. Ex.: '$BACKUP_S3_BUCKET': ";
        read
        if [[ ! -z ${REPLY} ]]; then
            BACKUP_S3_BUCKET=${REPLY}
        fi

        echo ""
        AWS_CONFIG_PATH="$(echo $HOME)/.aws"
        echo -n "Type path of AWS credentials [$AWS_CONFIG_PATH]: ";
        read
        if [[ ! -z ${REPLY} ]]; then
            AWS_CONFIG_PATH=${REPLY}
        fi

        echo ""
        AWS_CONFIG_PROFILE='default'
        echo -n "Type profile of AWS credentials [$AWS_CONFIG_PROFILE]: ";
        read
        if [[ ! -z ${REPLY} ]]; then
            AWS_CONFIG_PROFILE=${REPLY}
        fi

        echo ""
        BACKUP_FILE_NAME="db-calculadora_artesanatos"
        echo -n "Type file name of backup [$BACKUP_FILE_NAME]: ";
        read
        if [[ ! -z ${REPLY} ]]; then
            BACKUP_FILE_NAME=${REPLY}
        fi

        echo ""
        PROJECT_DB_NAME="app_db"
        echo -n "Type database name [$PROJECT_DB_NAME]: ";
        read
        if [[ ! -z ${REPLY} ]]; then
            PROJECT_DB_NAME=${REPLY}
        fi

        echo ""
        PROJECT_DB_USER="app_dbuser"
        echo -n "Type database user [$PROJECT_DB_USER]: ";
        read
        if [[ ! -z ${REPLY} ]]; then
            PROJECT_DB_USER=${REPLY}
        fi

        echo ""
        echo "--------------------------------------------------------------------------------"
        echo ""
        echo "INFO: This setting will be stored in $env_restore_file and can be changed at any time."
        echo ""
        echo -n "Type ENTER to continue"
        read

        echo "BACKUP_S3_BUCKET=$BACKUP_S3_BUCKET" > $env_restore_file
        echo "AWS_CONFIG_PATH=$AWS_CONFIG_PATH" >> $env_restore_file
        echo "AWS_CONFIG_PROFILE=$AWS_CONFIG_PROFILE" >> $env_restore_file
        echo "PROJECT_DB_NAME=$PROJECT_DB_NAME" >> $env_restore_file
        echo "PROJECT_DB_USER=$PROJECT_DB_USER" >> $env_restore_file
        echo "BACKUP_FILE_NAME=$BACKUP_FILE_NAME" >> $env_restore_file
    fi

    echo ""
    echo "Download latest backup from AWS ..."
    container-engine run --rm -it -v $AWS_CONFIG_PATH:/root/.aws -v $(pwd):/aws amazon/aws-cli s3 --profile $AWS_CONFIG_PROFILE cp s3://$BACKUP_S3_BUCKET/$(date +'%u')/$BACKUP_FILE_NAME.sql.gz .

    echo ""
    echo "Unzip data DB ..."
    gzip -d $BACKUP_FILE_NAME.sql.gz

    echo ""
    echo "Reset DB ..."
    compose up -d
    container-engine stop $(container-engine ps -q -f name=$(django_project_name_filter)_web)
    container-engine exec -i $(container-engine ps -q -f name=$(django_project_name_filter)_db) psql -U ${PROJECT_DB_USER} -d postgres -c "drop database ${PROJECT_DB_NAME}"
    container-engine exec -i $(container-engine ps -q -f name=$(django_project_name_filter)_db) psql -U ${PROJECT_DB_USER} -d postgres -c "create database ${PROJECT_DB_NAME}"

    echo ""
    echo "Import data ..."
    cat $BACKUP_FILE_NAME.sql | container-engine exec -i $(container-engine ps -q -f name=$(django_project_name_filter)_db) psql ${PROJECT_DB_NAME} -U ${PROJECT_DB_USER}

    echo ""
    echo "Migrate ..."
    container-engine start $(container-engine ps -qa -f name=$(django_project_name_filter)_web)
    container-engine exec $(container-engine ps -q -f name=$(django_project_name_filter)_web) python manage.py migrate

    echo ""
    echo "clear tmp ..."
    rm -rf $BACKUP_FILE_NAME*

    echo ""
    echo "Create superuser 'useradmin'? 'CTRL C' to exit."
    container-engine exec -it $(container-engine ps -q -f name=$(django_project_name_filter)_web) python manage.py createsuperuser --user useradmin --email useradmin@domain
}

exec_pg_dump() {
    env_restore_file='.envs/.env.aws.restore'
    if [ ! -z $(ls $env_restore_file 2>/dev/null) ]; then
        source $env_restore_file
    else
        echo "Erro: Não foi encontrado o arquivo de configurações da base de dados $env_restore_file."
        echo "Execute o seguinte comando para configuração:"
        echo "./docker-django.sh restore db"
        exit 1;
    fi

    dump_file=/tmp/$BACKUP_FILE_NAME.sql.gz
    container-engine exec -i $(container-engine ps -q -f name=$(django_project_name_filter)_db) pg_dump -U ${PROJECT_DB_USER} -d ${PROJECT_DB_NAME}  | gzip --best > $dump_file
    echo $dump_file

}

help() {
    cat docker_django/script/README.md | awk -F'```' '{print $1}'
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            help
            exit;;
    esac
done

# make sure we actually *did* get passed a valid function name
if declare -f "$1" >/dev/null 2>&1; then
    # invoke that function, passing arguments through
    "$@" # same as "$1" "$2" "$3" ... for full argument list
else
    echo "Function $1 not recognized" >&2
    help
fi
