Calculadora Artesanatos
#######################

Calculadora do `Preço justo <https://artesol.org.br/files/uploads/downloads/Cartilha-Comercio-Justo.pdf>`_
com objetivo da valorização da cultura através dos trabalhos artesanais
das comunidades Guarani do litoral paulista.

Contexto
********
...

Funcionalidades
***************
...

Método de cálculo do preço justo
********************************
...

Dúvidas ou sugestões
********************
...

Manual de uso
*************
...

Configuração da calculadora
===========================
...

Uso da calculadora do preço justo
=================================
...

Reporte de erros
================
...

Manual desenvolvedor(a)
***********************
...

Composição do projeto
=====================
...

.. kroki:: ./compose_viz-project.dot png

Instalação
==========
* `Instalação via Docker / Podman <docker-instructions>`_
* `Instalação via Python Packages <installation>`_

Preço justo Django APP
======================

Esta é a aplicação principal da Calculadora do preço justo.

Nesta documentação você tem maiores informações acerca da estrutura da aplicação e modelo de dados.

* `Preço justo Django application <preco_justo_readme>`_

Como contribuir técnicamente
****************************
...

Licença
*******
...

Status do projeto
*****************
...
