from io import StringIO
import os
from os.path import join
import shutil
from pathlib import Path

from django.core.files import File
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect

from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import (
    CreateView, UpdateView, DeleteView
)
from django.http import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import mail_admins
from django.conf import settings
from django.urls import reverse

from django_fine_uploader.views import FineUploaderView
from django_fine_uploader.fineuploader import ChunkedFineUploader

from . import models
from . import forms
from . import helpers
from . import mixins


##########################
# Consulta dos resultados
# de variações combinadas
##########################

class ConsultaArtesanatoListView(TemplateView):
    template_name = 'preco_justo/consulta_artesanato/list.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        access_token = kwargs['access_token']

        # Filter by tecnica_artesanal
        tecnicas_artesanais_tekoa = models.TekoaTecnicaArtesanatoConfig.objects\
            .filter(tekoa__access_token__exact=access_token)
        ctx['tecnicas_artesanais'] = tecnicas_artesanais_tekoa
        tecnica_artesanal = {}
        [exec(f"{t}=''", {"tecnica_artesanal": tecnica_artesanal})
         for t in self.request.GET
         if 'tecnica_artesanal' in t]
        tecnicas_artesanais_selected = tecnica_artesanal.keys()
        ctx['tecnicas_artesanais_selected'] = tecnicas_artesanais_selected

        if tecnicas_artesanais_selected:
            tecnicas_artesanais = tecnicas_artesanais_tekoa.filter(
                tecnica_artesanato__id__in=tecnicas_artesanais_selected)
        else:
            tecnicas_artesanais = tecnicas_artesanais_tekoa

        # Filter by Tekoá
        ctx['tekoa_selected'] = tecnicas_artesanais_tekoa.first().tekoa
        ctx['tekoa_options'] = models.TekoaCadastro.objects.exclude(
            access_token__exact=access_token)

        # Session to tekoa access_token
        self.request.session['tekoa_access_token'] = ctx['tekoa_selected'].access_token

        if not tecnicas_artesanais:
            tecnica_artesanal_filter = models.TecnicaArtesanatoCadastro.objects.filter(
                id__in=tecnicas_artesanais_selected).values('nome')
            tecnica_artesanal_filter = ', '.join([t['nome'] for t in tecnica_artesanal_filter])
            ctx['filter_empty_msg'] = \
                f'Nenhum artesanato encontrado para técnica "{ tecnica_artesanal_filter }".'

        # List object artesanato from helper
        artesanatos = []
        for tecnica_artesanal in tecnicas_artesanais:
            variacoes_combinadas = tecnica_artesanal.combine_variacoes()

            for combinacao in variacoes_combinadas:
                consulta_artesanato = helpers.ConsultaArtesanato(combinacao)
                artesanatos.append(consulta_artesanato)

        ctx['artesanatos'] = artesanatos

        return ctx


class ConsultaArtesanatoCalculoDetailView(TemplateView):
    template_name = 'preco_justo/consulta_artesanato/_calculo_detail.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        access_token = kwargs.get('access_token')
        combinacao_list = kwargs.get('variacoes').split('-')
        combinacao = models.TekoaTecnicaArtesanatoVariacaoValores.objects.filter(
            id__in=combinacao_list,
            tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tekoa__access_token=access_token
        )
        if not combinacao:
            return ctx
        consulta_artesanato = helpers.ConsultaArtesanato(combinacao)
        ctx['object'] = consulta_artesanato
        ctx['tempo_trabalho_config'] =\
            models.TempoTrabalhoConfig.objects.first()
        ctx['custos_relacionados'] = consulta_artesanato.fatores_custos()

        return ctx


class ConsultaArtesanatoFotoCreateView(LoginRequiredMixin, TemplateView):
    template_name = 'preco_justo/consulta_artesanato/_foto_form.html'

    # TODO: consistir para artesã adicionar foto apenas de sua tekoa

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        access_token = self.kwargs.get('access_token')
        # import pdb; pdb.set_trace()
        combinacao_list = self.kwargs.get('variacoes').split('-')
        combinacao = models.TekoaTecnicaArtesanatoVariacaoValores.objects.filter(
            id__in=combinacao_list,
            tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tekoa__access_token=access_token
        )
        if not combinacao:
            return ctx
        consulta_artesanato = helpers.ConsultaArtesanato(combinacao)
        ctx['consulta_artesanato'] = consulta_artesanato

        return ctx


class ConsultaArtesanatoFotoFineUploaderView(LoginRequiredMixin,
                                             mixins.FineUploaderViewFix):
    """Example of a chunked, but NOT concurrent upload.
    Disabling concurrent uploads per view.

    Remember, you can turn off concurrent uploads on your settings, with:
    FU_CONCURRENT_UPLOADS = False
    """
    @property
    def concurrent(self):
        return False

    def form_valid(self, form):
        self.process_upload(form)
        data = {'success': True}
        if self.upload.finished:
            path = Path('/'.join([
                settings.MEDIA_ROOT,
                self.upload.file_path,
                self.upload.filename]))
            access_token = self.kwargs['access_token']
            variacoes_list = self.kwargs['variacoes'].split('-')
            variacoes = models.TekoaTecnicaArtesanatoVariacaoValores.objects.filter(
                tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tekoa__access_token=access_token,
                id__in=variacoes_list,
            )
            if not variacoes:
                # TODO return error
                pass

            with path.open(mode="rb") as f:
                instance = models.ConsultaArtesanatoFoto.objects.create(
                    tekoa = variacoes[0].tekoa_tecnica_variacao.tekoa_tecnica_artesanato.tekoa,
                    user_add = self.request.user,
                )
                # for variacao in variacoes:
                instance.combinacao_variacao.set(variacoes)
                instance.foto=File(f, name=path.name)
                instance.save()
            os.remove(path)
            os.rmdir(settings.MEDIA_ROOT + '/' + self.upload.file_path)
        return self.make_response(data)


class ConsultaArtesanatoFotoDeleteView(LoginRequiredMixin, DeleteView):
    model = models.ConsultaArtesanatoFoto

    def get_queryset(self):
        # check auth
        access_token=self.kwargs['access_token']
        if not self.request.user.is_authenticated:
            return qs.none()
        if (not self.request.user.is_superuser and
                self.request.user.tekoa.access_token != access_token):
            return qs.none()

        qs = super().get_queryset()

        # filtrar por tekoa.access_token
        qs = qs.filter(
            tekoa__access_token=access_token
        )

        # filtrar por variacoes
        variacoes = self.kwargs.get('variacoes', []).split('-')
        for variacao_id in variacoes:
            qs = qs.filter(combinacao_variacao=variacao_id)

        return qs

    def get_success_url(self):
        access_token = self.request.session.get('tekoa_access_token');
        return reverse('consulta-artesanato-list', args=[access_token])


#########################
# Cadastro de artesanato
#########################

class ArtesanatoListView(LoginRequiredMixin, ListView):
    model = models.ArtesanatoCadastro
    paginate_by = 100

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(artesa=self.request.user)
        qs = qs.select_related(
            'tekoa',
            'artesa',
            'tekoa_tecnica_artesanato',
            'tekoa_tecnica_artesanato__tecnica_artesanato',
        )
        qs = qs.prefetch_related(
            'fotos',
            'variacoes',
            'variacoes__valor_variacao',
        )
        return qs


class PublicacaoArtesanatoListView(ListView):
    model = models.ArtesanatoCadastro
    paginate_by = 100
    template_name_suffix = '_publicacao_list'

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(data_publicacao__isnull=False)
        qs = qs.select_related(
            'tekoa',
            'artesa',
            'tekoa_tecnica_artesanato',
            'tekoa_tecnica_artesanato__tecnica_artesanato',
        )
        qs = qs.prefetch_related(
            'fotos',
            'variacoes',
            'variacoes__valor_variacao',
        )
        return qs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        ctx['artesanato_publicacao_btn_actions'] = True
        ctx['target'] = 'favoritos'

        return ctx


class ArtesanatoDetailViewBase(DetailView):
    model = models.ArtesanatoCadastro

    def get(self, request, *args, **kwargs):
        try:
            self.object=self.get_object()
        except:
            messages.warning(request, 'O artesanato que tentou acessar não existe mais.')
            return redirect('artesanato-list')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tempo_trabalho_config'] =\
                models.TempoTrabalhoConfig.objects.first()

        context['custos_relacionados'] = self.fatores_custos()

        return context

    def fatores_custos(self):
        fatores_custos = []
        for custo in self.object.tekoa_tecnica_artesanato.custos_relacionados.all():
            fator_rentabilidade_por_opcao_de_variacao=\
                self.object.fator_rentabilidade_por_opcao_de_variacao(custo, return_object=True)
            if fator_rentabilidade_por_opcao_de_variacao:
                obj_fator = fator_rentabilidade_por_opcao_de_variacao
                fator_custo = f"{ custo.custo } - R$ { custo.valor } - \
                    Rende { obj_fator.rentabilidade_por_peca_base } peças-base"
            else:
                fator_custo = f"{ custo.custo } - R$ { custo.valor } - Rende { custo.rentabilidade_por_peca_base } peças-base"
            fatores_custos.append(fator_custo)
        return fatores_custos


class ArtesanatoDetailView(LoginRequiredMixin, ArtesanatoDetailViewBase):

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(artesa=self.request.user)
        if not qs:
            qs = models.ArtesanatoCadastro.objects.none()
        return qs


class ArtesanatoPrecoJustoComposicaoView(LoginRequiredMixin, ArtesanatoDetailViewBase):
    template_name_suffix = '_preco_justo_composicao'

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(artesa=self.request.user)
        qs = qs.select_related(
            'tekoa',
            'artesa',
            'tekoa_tecnica_artesanato',
            'tekoa_tecnica_artesanato__tecnica_artesanato',
        )
        qs = qs.prefetch_related(
            'fotos',
            'variacoes',
            'variacoes__valor_variacao',
        )
        if not qs:
            qs = models.ArtesanatoCadastro.objects.none()
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['nav'] = 'preco_justo'

        return context


class PublicacaoArtesanatoDetailView(ArtesanatoDetailViewBase):
    template_name_suffix = '_publicacao_detail'

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(data_publicacao__isnull=False)
        return qs

    def post(self, request, *args, **kwargs):
        subject = 'Interesse no artesanato'
        message = 'Estive navegando pelo Catálogo Tekoá Kuery e me interessei por esta peça: \n'
        message += request.build_absolute_uri()
        message += '\n\nMeus dados de contato: \n'
        message += request.POST.get('email', '') + ' | '
        message += request.POST.get('telefone', '')

        mail_admins(
            subject,
            message,
            fail_silently=False,
        )

        self.object = self.get_object()
        kwargs['feedback_message'] = "Sua mensagem foi enviada com sucesso, obrigado!"
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class ArtesanatoCreateView(LoginRequiredMixin, CreateView):
    form_class = forms.ArtesanatoCreateModelForm
    model = form_class.Meta.model

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['formset'] = None

        return context

    def get_success_url(self):
        url = reverse('artesanato-update', args=[self.object.id])
        try:
            url += f'?msg={self.feedback_msg}'
        except:
            pass
        return url

    def form_valid(self, form):
        self.object = form.save()
        self.object.artesa_id = self.request.user.id
        self.object.tekoa_id = self.request.user.tekoa.id
        self.object.save()

        self.feedback_msg = 'Artesanato adicionado com sucesso!'
        self.feedback_msg += '\n\nAgora selecione as características de variações.'

        return super().form_valid(form)


class ArtesanatoUpdateView(LoginRequiredMixin, UpdateView):
    form_class = forms.ArtesanatoUpdateModelForm
    model = form_class.Meta.model

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except:
            messages.warning(self.request, 'O artesanato que tentou acessar não existe mais.')
            return redirect('artesanato-list')

        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(artesa=self.request.user)
        qs = qs.select_related(
            'tekoa',
            'artesa',
            'tekoa_tecnica_artesanato',
            'tekoa_tecnica_artesanato__tecnica_artesanato',
        )
        qs = qs.prefetch_related(
            'variacoes',
            'variacoes__valor_variacao',
        )
        if not qs:
            qs = models.ArtesanatoCadastro.objects.none()
        return qs

    def get_success_url(self):
        url = reverse('artesanato-update', args=[self.object.pk])
        try:
            url += f'?msg={self.feedback_msg}'
        except:
            pass
        return url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Atualiza variações relacionadas ao artesanato
        self.object.variacoes_init()

        context['form'].fields['tekoa_tecnica_artesanato'].queryset=\
                models.TekoaTecnicaArtesanatoConfig.objects\
                .filter(tekoa=self.request.user.tekoa)

        variacoes_selecionadas = self.object.variacoes.filter(valor_variacao__isnull=False)
        context['variacoes_selecionadas'] = [v.valor_variacao.id for v in variacoes_selecionadas]

        try:
            context['formset'] = forms.ArtesanatoFormSet(instance=self.object)
        except:
            context['formset'] = forms.ArtesanatoFormSet()

        return context

    def form_valid(self, form):
        self.object = form.save()
        self.object.artesa_id = self.request.user.id
        self.object.tekoa_id = self.request.user.tekoa.id
        self.object.save()

        formset = forms.ArtesanatoFormSet(
                self.request.POST, self.request.FILES,
                instance=self.object)
        if formset.is_valid():
            if formset.has_changed():
                self.object.set_data_modificacao()
                self.feedback_msg = 'Variações gravadas com sucesso!'
                if not self.object.fotos.count():
                    self.feedback_msg += '\n\nAgora adicione fotos ao cadastro do artesanato.'

            formset.save()

        if form.is_valid():
            return super().form_valid(form)
        else:
            return super().form_invalid(form)


class ArtesanatoFotoListView(LoginRequiredMixin, ListView):
    model = models.ArtesanatoFoto
    paginate_by = 100

    def get(self, request, *args, **kwargs):
        try:
            artesanato=models.ArtesanatoCadastro.objects.get(id=self.kwargs['pk'])
        except:
            messages.warning(self.request, 'O artesanato que tentou acessar não existe mais.')
            return redirect('artesanato-list')

        fotos = artesanato.fotos.filter(destaque=True)
        if fotos.count() > 1:
            foto_destaque = fotos.first()
            artesanato.fotos.update(destaque=False)
            foto_destaque.destaque=True
            foto_destaque.save()
        if artesanato.fotos.count() > 0 and fotos.count() == 0:
            foto_destaque = artesanato.fotos.first()
            foto_destaque.destaque=True
            foto_destaque.save()

        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(artesanato__id=self.kwargs['pk'])
        qs = qs.select_related(
            'artesanato',
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['nav'] = 'fotos'
        context['object'] = models.ArtesanatoCadastro.objects\
            .select_related(
                'tekoa',
                'artesa',
                'tekoa_tecnica_artesanato',
                'tekoa_tecnica_artesanato__tecnica_artesanato',
            )\
            .prefetch_related(
                'fotos',
                'variacoes',
                'variacoes__valor_variacao',
            )\
            .get(pk=self.kwargs['pk'])


        return context


class ArtesanatoFotoCreateView(LoginRequiredMixin, CreateView):
    form_class = forms.ArtesanatoFotoModelForm
    model = form_class.Meta.model
    # template_name = 'preco_justo/artesanatofoto_test_uploader.html'

    def get_success_url(self):
        url = reverse('artesanato-fotos', args=[self.object.artesanato.id])
        try:
            url += f'?msg={self.feedback_msg}'
        except:
            pass
        return url

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(self.kwargs)
        return kwargs

    def get(self, request, *args, **kwargs):
        artesanato_id = self.kwargs['pk']
        try:
            artesanato = models.ArtesanatoCadastro.objects.get(id=artesanato_id)
        except:
            messages.warning(self.request, 'O artesanato que tentou acessar não existe mais.')
            return redirect('artesanato-list')

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['nav'] = 'fotos'
        context['object'] = models.ArtesanatoCadastro.objects\
            .select_related(
                'tekoa',
                'artesa',
                'tekoa_tecnica_artesanato',
                'tekoa_tecnica_artesanato__tecnica_artesanato',
            )\
            .prefetch_related(
                'fotos',
                'variacoes',
                'variacoes__valor_variacao',
            )\
            .get(pk=self.kwargs['pk'])

        return context

    def form_valid(self, form):
        super().form_valid(form)

        if form.has_changed:
            artesanato = self.object.artesanato
            artesanato.set_data_modificacao()
            self.feedback_msg = 'Foto adicionada com sucesso!'

        return super().form_valid(form)


class ArtesanatoFotoFineUploaderView(LoginRequiredMixin,
                                     mixins.FineUploaderViewFix):
    """Example of a chunked, but NOT concurrent upload.
    Disabling concurrent uploads per view.

    Remember, you can turn off concurrent uploads on your settings, with:
    FU_CONCURRENT_UPLOADS = False
    """
    @property
    def concurrent(self):
        return False

    def form_valid(self, form):
        self.process_upload(form)
        data = {'success': True}
        if self.upload.finished:
            path = Path('/'.join([
                settings.MEDIA_ROOT,
                self.upload.file_path,
                self.upload.filename]))
            artesanato_id=self.kwargs.get('pk')
            artesanato = models.ArtesanatoCadastro.objects.get(pk=artesanato_id)
            with path.open(mode="rb") as f:
                artesanato.fotos.create(
                    foto=File(f, name=path.name),
                )
            os.remove(path)
            os.rmdir(settings.MEDIA_ROOT + '/' + self.upload.file_path)
        return self.make_response(data)


class ArtesanatoDeleteView(LoginRequiredMixin, DeleteView):
    model = models.ArtesanatoCadastro

    """
    TODO Pensar melhor no delete de artesanato e publicação
    """

    def get_success_url(self):

        url = reverse('artesanato-list')
        url += '?msg=artesamato deletado com sucesso!'

        return url

    def form_valid(self, form):
        artesanato = self.object
        artesanato.deletar_publicacao()

        return super().form_valid(form)


class ArtesanatoFotoDeleteView(LoginRequiredMixin, DeleteView):
    model = models.ArtesanatoFoto

    def get_success_url(self):
        artesanato = self.object.artesanato
        artesanato.set_data_modificacao()

        return reverse('artesanato-fotos', args=[artesanato.id])


class ArtesanatoFotoDestaqueView(LoginRequiredMixin, DetailView):
    model = models.ArtesanatoFoto

    def set_foto_destaque(self):
        # desmarcar destaque para todas fotos do artesanato
        self.object.artesanato.fotos.all().update(destaque=False)
        self.object.destaque=True
        self.object.save()

        artesanato = self.object.artesanato
        artesanato.set_data_modificacao()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        self.set_foto_destaque()

        return context


class ArtesanatoPublicacaoView(LoginRequiredMixin, TemplateView):
    template_name = 'preco_justo/artesanatocadastro_publicacao.html'

    def get(self, request, *args, **kwargs):
        try:
            self.object=self.get_object()
        except:
            messages.warning(self.request, 'O artesanato que tentou acessar não existe mais.')
            return redirect('artesanato-list')

        return super().get(request, *args, **kwargs)

    def get_object(self):
        return models.ArtesanatoCadastro.objects\
            .select_related(
                'tekoa',
                'artesa',
                'tekoa_tecnica_artesanato',
                'tekoa_tecnica_artesanato__tecnica_artesanato',
            )\
            .prefetch_related(
                'fotos',
                'variacoes',
                'variacoes__valor_variacao',
            )\
            .get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        self.object = self.get_object()
        context['object'] = self.object

        context['nav'] = 'publicacao'

        context['status_publicacao'], context['mensagem_publicacao'] = \
            self.object.get_status_publicacao()

        return context

    def post(self, request, pk, *args, **kwargs):
        self.object = self.get_object()
        acao = request.POST.get('acao')
        mensagem = request.POST.get('mensagem')
        host = request.build_absolute_uri('/')[:-1]
        if acao == 'PUBLICAR':
            self.object.publicar_artesanato(mensagem, host)
        elif acao == 'ATUALIZAR_PUBLICACAO':
            self.object.atualizar_publicacao(mensagem, host)
        elif acao == 'RETIRAR_PUBLICACAO':
            self.object.retirar_publicacao(mensagem, host)

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class PrecoJustoOpcoes(ArtesanatoDetailView):
    template_name_suffix = '_preco_opcoes'


@csrf_exempt
def artesanato_favorite(request, pk):
    if request.method != 'POST':
        return JsonResponse({
            'status': 'error',
            'msg':'Desculpe, não foi possível favoritar o artesanato',
            'console':'Erro: método não disponível!',
        })

    if not request.session.get('artesanatos_favoritos', False):
        request.session['artesanatos_favoritos'] = {}
    artesanato_id = str(pk)
    if not artesanato_id:
        raise 'artesanato não informado!'

    artesanatos_favoritos = request.session['artesanatos_favoritos']
    if artesanato_id in request.session['artesanatos_favoritos']:
        # desfavoritar
        del request.session['artesanatos_favoritos'][artesanato_id]
        action = 'desfavoritado'
        msg = 'Desfavoritado!'
    else:
        request.session['artesanatos_favoritos'][artesanato_id]=True
        action = 'favoritado'
        msg = 'Favorito!'

    request.session.modified = True

    return JsonResponse({
        'status': 'ok',
        'action': action,
        'msg':msg,
        'fav_count': len(request.session['artesanatos_favoritos']),
    })


class PublicacaoArtesanatoFavoriteView(mixins.AjaxableResponseMixin, CreateView):
    form_class = forms.ArtesanatoFavoritoContactForm
    model = form_class.Meta.model
    template_name = 'preco_justo/artesanatocadastro_publicacao_favoritos.html'
    artesanatos_favoritos = None

    def get_artesanatos_favoritos(self):
        if 'artesanatos_favoritos' not in self.request.session:
            return None

        if self.artesanatos_favoritos:
            return self.artesanatos_favoritos

        self.artesanatos_favoritos = models.ArtesanatoCadastro.objects\
            .select_related(
                'tekoa',
                'artesa',
                'tekoa_tecnica_artesanato',
                'tekoa_tecnica_artesanato__tecnica_artesanato',
            )\
            .prefetch_related(
                'fotos',
                'variacoes',
                'variacoes__valor_variacao',
            )\
            .filter(
                id__in=self.request.session['artesanatos_favoritos'],
                data_publicacao__isnull=False
            )
        return self.artesanatos_favoritos

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        ctx['artesanatos_favoritos'] = self.get_artesanatos_favoritos()
        ctx['target'] = 'artesanatos_favoritos'

        return ctx

    def get_success_url(self):
        return reverse('publico-artesanato-favorite-list')

    def send_message_admins(self):
        subject = 'Interesse no artesanato'
        message = 'Estive navegando pelo Catálogo Tekoá Kuery e me interessei por esta peça: \n'
        message += self.request.build_absolute_uri()
        message += '\n\nMeus dados de contato: \n'
        message += self.request.POST.get('email', '') + ' | '
        message += self.request.POST.get('telefone', '')

        mail_admins(
            subject,
            message,
            fail_silently=False,
        )

    def form_valid(self, form):
        # check fav session
        artesanatos_favoritos = self.get_artesanatos_favoritos()
        if not artesanatos_favoritos:
            form.add_error('', 'Por favor, selecione os artesanatos favoritos antes de enviar o formulário!')
            return self.form_invalid(form)

        self.object = form.save()

        self.object.artesanatos.set(artesanatos_favoritos)

        self.send_message_admins()
        # TODO:
        # self.send_message_interessada()
        # self.send_message_tekoa()
        # self.send_message_artesa()

        del self.request.session['artesanatos_favoritos']

        return super().form_valid(form)


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Sua senha foi atualizada com sucesso!')
            return redirect('artesa_change_password')
        else:
            messages.error(request, 'Por favor, corrija o erro abaixo.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'preco_justo/change_password.html', {
        'form': form
    })
