from dal import autocomplete

from . import models


class TecnicaArtesanalAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.TekoaTecnicaArtesanatoConfig.objects.none()

        qs = models.TekoaTecnicaArtesanatoConfig\
            .objects.filter(tekoa=self.request.user.tekoa)

        if self.q:
            qs = qs.filter(
                tecnica_artesanato__nome__icontains=self.q)

        return qs

    def get_result_label(self, item):
        return item.tecnica_artesanato.nome

    def get_selected_result_label(self, item):
        return item.tecnica_artesanato.nome


class VariacaoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.TekoaTecnicaArtesanatoVariacaoCadastro.objects.none()

        qs = models.TekoaTecnicaArtesanatoVariacaoCadastro\
            .objects.filter(
                tekoa_tecnica_artesanato__tekoa=self.request.user.tekoa
            )

        if self.q:
            qs = qs.filter(
                nome__icontains=self.q)

        return qs

    def get_result_label(self, item):
        return item.nome

    def get_selected_result_label(self, item):
        return item.nome


class VariacaoValoresAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.TekoaTecnicaArtesanatoVariacaoValores.objects.none()

        qs = models.TekoaTecnicaArtesanatoVariacaoValores\
            .objects.filter(
                tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tekoa=self.request.user.tekoa
            )

        variacao = self.forwarded.get('variacao', None)

        if variacao:
            qs = qs.filter(tekoa_tecnica_variacao=variacao)

        if self.q:
            qs = qs.filter(
                valor_variacao__icontains=self.q)

        return qs

    def get_result_label(self, item):
        return item.valor_variacao

    def get_selected_result_label(self, item):
        return item.valor_variacao
