from io import StringIO
import os
from os.path import join
import shutil
from pathlib import Path

from django.http import JsonResponse
from django.core.files import File
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect

from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import (
    CreateView, UpdateView, DeleteView
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import mail_admins
from django.conf import settings
from django.urls import reverse

from django_fine_uploader.views import FineUploaderView
from django_fine_uploader.fineuploader import ChunkedFineUploader

from . import models
from . import forms


###################
# FineUploaderView
###################

class ChunkedFineUploaderFix(ChunkedFineUploader):
    def combine_chunks(self):
        """Combine a chunked file into a whole file again. Goes through each part,
        in order, and appends that part's bytes to another destination file.

        Discover where the chunks are stored in settings.CHUNKS_DIR
        Discover where the uploads are saved in settings.UPLOAD_DIR
        """
        # So you can see I'm saving a empty file here. That way I'm able to
        # take advantage of django.core.files.storage.Storage.save (and
        # hopefully any other custom Django storage). In a nutshell the
        # ``final_file`` will get a valid name
        # django.core.files.storage.Storage.get_valid_name
        # and I don't need to create some dirs along the way to open / create
        # my ``final_file`` and write my chunks on it.
        # https://docs.djangoproject.com/en/dev/ref/files/storage/#django.core.files.storage.Storage.save
        # In my experience with, for example, custom AmazonS3 storages, they
        # implement the same behaviour.
        self.real_path = self.storage.save(self._full_file_path, StringIO())

        with self.storage.open(self.real_path, 'wb') as final_file:
            for i in range(self.total_parts):
                part = join(self.chunks_path, str(i))
                with self.storage.open(part, 'rb') as source:
                    final_file.write(source.read())
        shutil.rmtree(self._abs_chunks_path)

    @property
    def chunked(self):
        total_parts = self.total_parts
        if not total_parts:
            total_parts=1
        return total_parts > 1


class FineUploaderViewFix(FineUploaderView):

    def process_upload(self, form):
        self.upload = ChunkedFineUploaderFix(form.cleaned_data, self.concurrent)
        if self.upload.concurrent and self.chunks_done:
            self.upload.combine_chunks()
        else:
            self.upload.save()


class AjaxableResponseMixin:

    ajax_response = {}

    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(
                {'errors': form.errors,
                 'errors_keys': [e for e in form.errors]},
                status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        try:
            response = super().form_valid(form)
        except Exception as e:
            if self.request.is_ajax():
                return JsonResponse({'errors': e.args[0]}, status=400)
            else:
                return e.args[0]

        if self.request.is_ajax():
            try:
                data = {
                    'pk': self.object.id
                }
            except:
                data = {}

            if not self.ajax_response:
                return JsonResponse(data)

            data['actions'] = ""

            return JsonResponse(data)

        else:
            return response

