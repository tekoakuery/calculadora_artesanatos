from . import models


class ConsultaArtesanatoBase(object):

    def __init__(self, variacoes_combinadas):
        self.variacoes = variacoes_combinadas
        self.tekoa_tecnica_artesanato = \
            variacoes_combinadas[0].tekoa_tecnica_variacao.tekoa_tecnica_artesanato
        self.tecnica_artesanal = self.tekoa_tecnica_artesanato.tecnica_artesanato
        self.tekoa = self.tekoa_tecnica_artesanato.tekoa
        self.combinacao_variacoes_verbose = ' - '.join([str(v.valor_variacao) for v in variacoes_combinadas])
        self.combinacao_variacoes_list = [str(v.id) for v in variacoes_combinadas]
        self.combinacao_variacoes_slug = '-'.join(self.combinacao_variacoes_list)


class ConsultaArtesanatoPrecoJusto(ConsultaArtesanatoBase):

    @property
    def preco_justo(self):
        return self.calculo_preco_justo()

    @property
    def preco_social(self):
        return self.calculo_preco_justo_desconto()

    def calculo_preco_justo_desconto(self):
        # TODO parametrizar percent_desconto em calculo_preco_justo_desconto
        percent_desconto = 50
        preco_justo = self.calculo_preco_justo()
        preco_justo_desconto = (preco_justo * percent_desconto) / 100
        preco_justo_desconto = float(preco_justo_desconto)
        return round(preco_justo_desconto, 2)

    def calculo_preco_justo(self):
        calculo = self.calculo_total_custos() + self.calculo_total_valor_tempo_trabalho()
        preco_justo = float(calculo)
        return round(preco_justo, 2)

    def calculo_total_custos(self):
        total_custos = self.calculo_total_custos_artesanato() + self.calculo_total_custos_variacoes()
        return round(total_custos, 2)

    def calculo_total_custos_artesanato(self):
        custos_artesanato=self.tekoa_tecnica_artesanato.custos_relacionados.all()
        soma_custos_artesanato=0
        for custo_artesanato in custos_artesanato:

            fator_rentabilidade_opcao_variacao =\
                self.fator_rentabilidade_por_opcao_de_variacao(custo_artesanato)
            if fator_rentabilidade_opcao_variacao:
                fator_rentabilidade=\
                    fator_rentabilidade_opcao_variacao
            else:
                # rendimento padrão: obtida do cadastro da técnica
                fator_rentabilidade=\
                    custo_artesanato.rentabilidade_por_peca_base

            if fator_rentabilidade:
                soma_custos_artesanato += custo_artesanato.valor / fator_rentabilidade
            else:
                soma_custos_artesanato += custo_artesanato.valor
        return soma_custos_artesanato

    def calculo_total_custos_variacoes(self):
        variacoes = self.variacoes
        total_custo_variacoes = 0
        for variacao in variacoes:
            soma_custo_variacao = 0
            for custo_variacao in variacao.custos.all():
                if custo_variacao.rentabilidade_por_peca_base:
                    soma_custo_variacao += custo_variacao.valor / custo_variacao.rentabilidade_por_peca_base
                else:
                    soma_custo_variacao += custo_variacao.valor
            total_custo_variacoes += soma_custo_variacao
        return total_custo_variacoes

    def calculo_total_valor_tempo_trabalho(self):
        total_tempo_trabalho = self.calculo_total_tempo_trabalho() * self.calculo_valor_hora()
        return round(total_tempo_trabalho, 2)

    def calculo_total_tempo_trabalho(self):
        tempo_trabalho = self.tekoa_tecnica_artesanato.tempo_trabalho_peca_base

        for variacao in self.variacoes:
            if variacao.tempo_trabalho_variacao:
                tempo_trabalho += variacao.tempo_trabalho_variacao

        if not tempo_trabalho:
            # TODO checar o que fazer caso tempo_trabalho = None
            return 0

        return tempo_trabalho

    @property
    def total_tempo_trabalho_em_dias(self):
        total = self.calculo_total_tempo_trabalho()
        if total < 8:
            return 0
        if total == 8:
            return '1 dia'
        dias = total / 8
        dias_int = int(dias)
        if dias == dias_int:
            return f'{dias_int} dias'
        else:
            if dias_int == 1:
                return f'mais de {dias_int} dia'
            return f'mais de {dias_int} dias'

    def calculo_valor_hora(self):
        tempo_trabalho_config = models.TempoTrabalhoConfig.objects.first()
        valor_hora = tempo_trabalho_config.piso_categoria_artesa / tempo_trabalho_config.carga_horas_mes
        return round(valor_hora, 2)

    def fator_rentabilidade_por_opcao_de_variacao(self, custo_artesanato, return_object=False):
        '''
          Verificar rendimento do material
          necessário para a tecnica artesanal
          aplicada à opção de variação.
          Este fator de rendimento será priorizado!
        '''
        variacoes_valores = [
            variacao.id
            for variacao in self.variacoes
        ]
        # TODO checar se não forma unique_together
        obj_rentabilidade = models.TekoaTecnicaArtesanatoVariacaoValorRentabilidade\
                .objects.filter(
                    tekoa_tecnica_variacao_valor__in=variacoes_valores,
                    custo=custo_artesanato
                )\
                .select_related(
                    'custo',
                    'tekoa_tecnica_variacao_valor')\
                .order_by('rentabilidade_por_peca_base') # ordenação prioriza rentabilidade menor
        if obj_rentabilidade:
            if return_object:
                return obj_rentabilidade.first()
            else:
                return obj_rentabilidade.first().rentabilidade_por_peca_base
        else:
            return None


class ConsultaArtesanatoCalculoDetail(ConsultaArtesanatoBase):

    def fatores_custos(self):
        fatores_custos = []
        for custo in self.variacoes[0]\
                .tekoa_tecnica_variacao\
                .tekoa_tecnica_artesanato\
                .custos_relacionados.all():
            fator_rentabilidade_por_opcao_de_variacao=\
                self.fator_rentabilidade_por_opcao_de_variacao(custo, return_object=True)
            if fator_rentabilidade_por_opcao_de_variacao:
                obj_fator = fator_rentabilidade_por_opcao_de_variacao
                fator_custo = f"{ custo.custo } - R$ { custo.valor } - \
                    Rende { obj_fator.rentabilidade_por_peca_base } peças-base"
            else:
                fator_custo = f"{ custo.custo } - R$ { custo.valor } - Rende { custo.rentabilidade_por_peca_base } peças-base"
            fatores_custos.append(fator_custo)
        return fatores_custos



class ConsultaArtesanatoFoto(ConsultaArtesanatoBase):

    @property
    def fotos(self):
        qs = models.ConsultaArtesanatoFoto.objects.filter(tekoa=self.tekoa)
        for variacao_id in self.combinacao_variacoes_list:
            qs = qs.filter(combinacao_variacao=variacao_id)
        return qs


class ConsultaArtesanato(ConsultaArtesanatoPrecoJusto,
                         ConsultaArtesanatoCalculoDetail,
                         ConsultaArtesanatoFoto):
    pass
