from django.urls import path
from . import views
from . import autocomplete


#########################
# Cadastro de Artesanato
#########################

urlpatterns = [
    path('',
         views.ArtesanatoListView.as_view(),
         name="artesanato-list"),

    path('artesanato/add/',
         views.ArtesanatoCreateView.as_view(),
         name="artesanato-create"),

    path('artesanato/<int:pk>/detail/',
         views.ArtesanatoDetailView.as_view(),
         name="artesanato-detail"),

    path('artesanato/<int:pk>/preco_justo/',
         views.ArtesanatoPrecoJustoComposicaoView.as_view(),
         name="artesanato-preco-justo-composicao"),

    path('artesanato/<int:pk>/update/',
         views.ArtesanatoUpdateView.as_view(),
         name="artesanato-update"),

    # TODO resolver melhor o delete
    path('artesanato/<int:pk>/delete/',
         views.ArtesanatoDeleteView.as_view(),
         name="artesanato-delete"),

    path('artesanato/<int:pk>/fotos/',
         views.ArtesanatoFotoListView.as_view(),
         name="artesanato-fotos"),
    path('artesanato/<int:pk>/foto/add/',
         views.ArtesanatoFotoCreateView.as_view(),
         name="artesanato-foto-create"),
    path('artesanato/<int:pk>/foto/add/upload',
         views.ArtesanatoFotoFineUploaderView.as_view(),
         name="artesanato-foto-create-upload"),

    path('artesanato/<int:pk>/publicacao/',
         views.ArtesanatoPublicacaoView.as_view(),
         name="artesanato-publicacao"),
    path('artesanato/<int:pk>/publicacao/',
         views.ArtesanatoPublicacaoView.as_view(),
         name="artesanato-publicar"),
    path('artesanato/<int:pk>/publicacao/',
         views.ArtesanatoPublicacaoView.as_view(),
         name="artesanato-atualizar-publicacao"),

    path('artesanato/<int:pk>/preco_justo/',
         views.PrecoJustoOpcoes.as_view(),
         name="artesanato-preco_justo"),

    path('foto/delete/<int:pk>/',
         views.ArtesanatoFotoDeleteView.as_view(),
         name="artesanato-foto-delete"),
    path('foto/destaque/<int:pk>/',
         views.ArtesanatoFotoDestaqueView.as_view(),
         name="artesanato-foto-destaque"),

    path('password/',
         views.change_password,
         name='artesa_change_password'),
]


######################
# Visualização pública
######################

urlpatterns += [
    path('publicacao/artesanato/',
         views.PublicacaoArtesanatoListView.as_view(),
         name="publico-artesanato-list"),
    path('publicacao/artesanato/<int:pk>/',
         views.PublicacaoArtesanatoDetailView.as_view(),
         name="publico-artesanato-detail"),
    path('publicacao/artesanato/<int:pk>/favorite/',
         views.artesanato_favorite,
         name="publico-artesanato-favorite"),
    path('publicacao/artesanato/favorite/',
         views.PublicacaoArtesanatoFavoriteView.as_view(),
         name="publico-artesanato-favorite-list"),
]


#######################
# Consulta da artesã/o
#######################

urlpatterns += [
    # list
    path('consulta/<str:access_token>/',
         views.ConsultaArtesanatoListView.as_view(),
         name="consulta-artesanato-list"),

    # cálculo
    path('consulta/<str:access_token>/<slug:variacoes>/calculo/',
         views.ConsultaArtesanatoCalculoDetailView.as_view(),
         name="consulta-artesanato-calculo-detail"),

    # foto da combinação de variações de técnica artesanal
    path('consulta/<str:access_token>/<slug:variacoes>/foto/add/',
         views.ConsultaArtesanatoFotoCreateView.as_view(),
         name="consulta-artesanato-foto-create"),
    path('consulta/<str:access_token>/<slug:variacoes>/foto/add/upload/',
         views.ConsultaArtesanatoFotoFineUploaderView.as_view(),
         name="consulta-artesanato-foto-create-upload"),
    path('consulta/<str:access_token>/<slug:variacoes>/foto/delete/<int:pk>/',
         views.ConsultaArtesanatoFotoDeleteView.as_view(),
         name="consulta-artesanato-foto-delete"),
]


###################
# Autocomplete URLs
###################

urlpatterns += [
    path(
        'tecnica_artesanal-autocomplete/',
        autocomplete.TecnicaArtesanalAutocomplete.as_view(),
        name='tecnica_artesanal-autocomplete',
    ),
    path(
        'variacao-autocomplete/',
        autocomplete.VariacaoAutocomplete.as_view(),
        name='variacao-autocomplete',
    ),
    path(
        'variacao_valor-autocomplete/',
        autocomplete.VariacaoValoresAutocomplete.as_view(),
        name='variacao_valor-autocomplete',
    ),
]
