/*
 * generic Modal
  * */
$(document).on('click', '.is_modal', function (event) {
  event.preventDefault();

  url = $( this ).attr('href');

  selector = '#genericModal';

  $(selector).modal({
    // backdrop: false
  });
  $(selector).modal('show');
  $(selector + ' .modal-title').html($(this).attr('modal-title'));
  $(selector + ' .modal-body').html(' carregando ... ');
  $(selector + ' .modal-body').load(url);

  $(selector).modal('handleUpdate')
});

/*
 * compartilhar artesanato
  * */
$('.button-share').on('click', function(evt){
  var text = 'Confira este artesanato do Catálogo Tekoa Kuery'
  var url = $(this).attr('href');
  console.log(url);

  if (navigator.share) {
    navigator.share({
      title: 'Catálogo Tekoa Kuery',
      text: text,
      url: url,
    })
      .then(() => console.log('Successful share'))
      .catch((error) => console.log('Error sharing', error));
  } else {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(url).select();
    document.execCommand("copy");
    $temp.remove();
    alert('O endereço desse artesanato foi copiado! \n Use a função "Colar" para compartilhar esta página!');
  }
});

/*
 * favoritar artesanato
  * */
$(document).on('click', '.artesanato-favorite-action', function(evt){
  evt.preventDefault();
  let elem = this
  let url = $(elem).attr('href');
  $.post(url, function( data ) {
    if (data.status != 'ok') {
      alert(data.msg);
      console.log(data.console);
      return;
    }

    let icon_elem = $('i', elem).first();
    let fav_text = $(elem).prev('.artesanato-favorite-text');

    icon_elem.toggleClass('fav-selected');
    icon_elem.toggleClass('fav-unselected');

    btn_artesanato_favorito = $(elem).data('btnArtesanatoFavorito');
    if (btn_artesanato_favorito) {
      icon_main_elem = $('i', '#'+btn_artesanato_favorito);
      icon_main_elem.toggleClass('fav-selected');
      icon_main_elem.toggleClass('fav-unselected');
    }

    fav_text.hide();
    fav_text.text(data.msg);
    fav_text.fadeIn();
    setInterval(function() {
      fav_text.fadeOut();
    }, 1500)

    artesanatos_favoritos_count(data.fav_count);
  });
});

/*
 * artesanatos favoritos
  * */
function artesanatos_favoritos_count (fav_count) {
  // clean badge
  $('#fav-count').html('');

  // consult & set badge
  if (fav_count > 0) {
    tag_badge = '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill fav-badge">';
    tag_badge += fav_count;
    tag_badge += '<span class="visually-hidden">artesanatos favoritos</span>';
    tag_badge += '</span>';
    $('#fav-count').html(tag_badge);
  }
}

/*
 * enviar lista de artesanatos favoritos
  * */
function toTitleCase(str) {
  return str.replace(/(?:^|\s)\w/g, function(match) {
    return match.toUpperCase();
  });
}
function form_errors_clear (form) {
  $('.invalid-feedback', form).remove();
  $('.is-invalid', form).removeClass('is-invalid');
}
$(document).on('submit', '#artesanatos-favoritos-list', function (evt) {
  evt.preventDefault();

  let elem = $(this);

  form_errors_clear(elem);

  $.post(elem.attr('action'), elem.serialize())
    .done(function() {
      nome = toTitleCase($('#id_nome').val());

      html = '<div class="col">';
      html += '<p>'+nome+', agradecemos seu interesse em valorizar a cultura Guarani M\'Bya.</p>';
      html += '<p>Em breve você receberá informações sobre como adquirirar a peças artesanais.</p>';
      html += '<hr class="mt-4">';
      html += '<div class="text-end"><button class="btn btn-success text-end" data-bs-dismiss="modal">Entendi! Continuar navegando</button></div>';
      html += '</div>';
      $('#artesanatos-favoritos-row').html(html);

      artesanatos_favoritos_count(0);

      $('.fav-selected').addClass('fav-unselected');
      $('.fav-selected').removeClass('fav-selected');
    })
    .fail(function( jqXHR, textStatus, errorThrown) {
      let data = jqXHR.responseJSON;
      let errors = data.errors;

      $(data.errors_keys).each(function(index, el){
        if (eval('errors.'+el)) {
          $('#id_'+el).addClass('is-invalid');
          msg_error = eval('errors.'+el+'[0]');
          html_msg = '<p id="error_1_id_'+el+'" class="invalid-feedback"><strong>'+msg_error+'</strong></p>';
          $('#id_'+el).after(html_msg);
          $('#id_'+el).focus();
        }
      });
    })
});
$(document).on('keyup', 'input', function () {
  form = $(this).parents('form');
  form_errors_clear(form);
});

/*
 * tela de explicação de como adquirir os artesanatos
  * */
$(document).on('click', '.btn-artesanatos-como-adquirir', function (evt) {
  $('#artesanatos-como-adquirir').toggleClass('d-none');
  $('#artesanatos-favoritos-row').toggleClass('d-none');
});
