from django.apps import AppConfig


class PrecoJustoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'preco_justo'
    verbose_name = 'Preço Justo'
