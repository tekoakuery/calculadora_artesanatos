import os
from datetime import datetime
import itertools
from uuid import uuid4

from django.utils.text import slugify
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.core.mail import mail_managers

from easy_thumbnails.fields import ThumbnailerImageField

'''
TODO

* Nome em Guarani;
'''


#################
## Configurações
#################

class TekoaCadastro(models.Model):
    nome = models.CharField(max_length=100,
            null=False, blank=False)
    # slug = models.SlugField(null=True, unique=True,
    #                         help_text='Slug é um termo jornalístico. \
    #                         Um slug é um rótulo curto para algo, contendo apenas letras, \
    #                         números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    descricao = models.TextField(
            'descrição',
            null=True, blank=True)
    email = models.EmailField(
            'e-mail de contato',
            null=True, blank=True)
    access_token = models.CharField(
        max_length=20, unique=True,
        null=True, blank=True)

    class Meta:
        ordering = ['nome', ]
        verbose_name = "Cadastro de Tekoa"
        verbose_name_plural = "1 - Cadastros de Tekoa"

    def __str__(self):
        return f"{self.nome}"

    def save(self, *args, **kwargs):

        if not self.access_token:
            self.access_token = str(uuid4()).split('-')[0]
            self.save()

        super().save(*args, **kwargs)


class TecnicaArtesanatoCadastro(models.Model):
    nome = models.CharField(
            max_length=100,
            null=False, blank=False)
    # slug = models.SlugField(null=True, unique=True,
    #                         help_text='Slug é um termo jornalístico. \
    #                         Um slug é um rótulo curto para algo, contendo apenas letras, \
    #                         números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    descricao = models.TextField(
            'descrição',
            null=True, blank=True)

    class Meta:
        ordering = ['nome', ]
        verbose_name = "Cadastro de técnica artesanal"
        verbose_name_plural = "Cadastros de técnica artesanal"

    def __str__(self):
        return f"{self.nome}"


def find_all_combinations(items):
    keys = sorted(items)
    combinations = itertools.product(
        *(items[k] for k in keys)
    )
    combinations_list = list(combinations)
    return combinations_list

class TekoaTecnicaArtesanatoConfig(models.Model):
    tekoa = models.ForeignKey(
        TekoaCadastro, on_delete=models.PROTECT,
        null=False, blank=False,
        related_name='tecnicas_artesanais')
    tecnica_artesanato = models.ForeignKey(
        TecnicaArtesanatoCadastro, on_delete=models.PROTECT,
        null=False, blank=False, verbose_name='técnica artesanal',
        related_name='tekoa_relacionadas')
    tempo_trabalho_peca_base = models.DecimalField(
            'Tempo-trabalho para Peça-base (horas)',
            help_text='Informe em horas o tempo de produção da peça artesanal, sendo ela mais simples.',
            max_digits=10, decimal_places=2,
            blank=True, null=True)

    class Meta:
        unique_together = ['tekoa', 'tecnica_artesanato']
        verbose_name = "configuração de técnica artesanal"
        verbose_name_plural = "2 - Configurações de técnica artesanal"

    def __str__(self):
        return f"{self.tekoa} - {self.tecnica_artesanato}"

    def combine_variacoes(self):
        item = {}
        for variacao in self.variacoes_relacionadas.all():
            item[variacao.nome] = [
                v for v in variacao.valores_variacao.all()
            ]

        combinacoes = find_all_combinations(item)
        return combinacoes


class TekoaTecnicaArtesanatoCustos(models.Model):
    tekoa_tecnica_artesanato = models.ForeignKey(
        TekoaTecnicaArtesanatoConfig, on_delete=models.PROTECT,
        null=False, blank=False,
        related_name='custos_relacionados')
    custo = models.CharField(max_length=100,
            null=False, blank=False,
            help_text='Digite o nome do custo.\nEx.: combustível; compra de material; etc.')
    # slug = models.SlugField(null=True, unique=True,
    #                         help_text='Slug é um termo jornalístico. \
    #                         Um slug é um rótulo curto para algo, contendo apenas letras, \
    #                         números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    descricao = models.CharField(
            'descrição do custo',
            max_length=400,
            null=True, blank=True)
    valor = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=False, null=False)
    rentabilidade_por_peca_base = models.PositiveIntegerField(
            'rendimento por peça-base',
            help_text='Atribua a quantidade de itens que este custo pode render.\nEx.: O custo da gasolina para obter madeira para bichinhos pode render 15 peças',
            null=False, blank=False)

    class Meta:
        verbose_name = "Custo implicado à técnica artesanal"
        verbose_name_plural = "Custos implicados à técnica artesanal"

    def __str__(self):
        return f"{self.tekoa_tecnica_artesanato} - {self.custo}"


class TekoaTecnicaArtesanatoVariacaoCadastro(models.Model):
    tekoa_tecnica_artesanato = models.ForeignKey(
        TekoaTecnicaArtesanatoConfig, on_delete=models.CASCADE,
        null=False, blank=False,
        related_name='variacoes_relacionadas')
    nome = models.CharField(max_length=100,
            verbose_name='variação',
            null=False, blank=False)
    # slug = models.SlugField(null=True, unique=True,
    #                         help_text='Slug é um termo jornalístico. \
    #                         Um slug é um rótulo curto para algo, contendo apenas letras, \
    #                         números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    descricao = models.CharField(
            'descrição',
            max_length=400,
            null=True, blank=True)
    opcional = models.BooleanField(
        'variação opcional',
        help_text='Selecione este campo caso a variação não sirva todas tekoa.')

    class Meta:
        verbose_name = "variação relacionada a técnica artesanal"
        verbose_name_plural = "Variações relacionadas a técnica artesanal"

    def __str__(self):
        return f"{self.tekoa_tecnica_artesanato} - {self.nome}"


class TekoaTecnicaArtesanatoVariacaoValores(models.Model):
    tekoa_tecnica_variacao = models.ForeignKey(
        TekoaTecnicaArtesanatoVariacaoCadastro, on_delete=models.PROTECT,
        verbose_name='variação da técnica artesanal', null=False, blank=False,
        related_name='valores_variacao')
    valor_variacao = models.CharField(
            'opção da variação', max_length=100,
            null=False, blank=False)
    # slug_valor_variacao = models.SlugField(null=True, unique=True,
    #                         help_text='Slug é um termo jornalístico. \
    #                         Um slug é um rótulo curto para algo, contendo apenas letras, \
    #                         números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    tempo_trabalho_variacao = models.DecimalField(
        'Tempo-trabalho para variação (horas)',
        help_text='Informe em horas a diferença do tempo de produção da peça artesanal, entre a peça-base e esta variação.',
        max_digits=10, decimal_places=2,
        blank=True, null=True)
    fator_raridade = models.CharField(
        'fator de raridade', max_length=100,
        help_text='Descreva o fator de raridade.\n\
        Ex.: Este material é raro pois existe apenas em algumas regiões.',
        null=True, blank=True)

    class Meta:
        verbose_name = "opção da variação relacionada a técnica artesanal"
        verbose_name_plural = "3 - Opções da variação relacionada a técnica artesanal"

    def __str__(self):
        return f"{self.tekoa_tecnica_variacao} - {self.valor_variacao}"


class TekoaTecnicaArtesanatoVariacaoValorRentabilidade(models.Model):

    tekoa_tecnica_variacao_valor = models.ForeignKey(
        TekoaTecnicaArtesanatoVariacaoValores,
        verbose_name='item variação', on_delete=models.CASCADE,
        null=True, blank=True,
        related_name='artesanato_custos')
    custo = models.ForeignKey(
        TekoaTecnicaArtesanatoCustos,
        verbose_name='custo do artesanato', on_delete=models.CASCADE,
        null=True, blank=True,
        related_name='variacoes_valores')
    rentabilidade_por_peca_base = models.PositiveIntegerField(
            'rendimento por peça-base',
            help_text='Atribua a quantidade de itens que este custo pode render.\nEx.: O custo do pacote de missangas, no tamanho grande, pode render 5 peças',
            null=False, blank=False)

    def __str__(self):
        return f"{self.custo} - {self.custo.rentabilidade_por_peca_base} peças"

    class Meta:
        verbose_name = "Rendimento do material relacionado à técnica para esta opção de variação"
        verbose_name_plural = "Rendimento dos materiais exigidos nesta técnica para esta opção de variação"


class TekoaTecnicaArtesanatoVariacaoValorCustos(models.Model):
    tekoa_tecnica_variacao_valor = models.ForeignKey(
        TekoaTecnicaArtesanatoVariacaoValores,
        verbose_name='item variação', on_delete=models.PROTECT,
        null=False, blank=False,
        related_name='custos')
    custo = models.CharField(max_length=100,
            null=False, blank=False,
            help_text='Digite o nome do custo.\nEx.: combustível; compra de missangas; etc.')
    # slug = models.SlugField(null=True, unique=True,
    #                         help_text='Slug é um termo jornalístico. \
    #                         Um slug é um rótulo curto para algo, contendo apenas letras, \
    #                         números, sublinhados ou hifens. Eles geralmente são usados em URLs.')
    descricao = models.CharField(
            'descrição do custo',
            max_length=400,
            null=True, blank=True)
    valor = models.DecimalField(
        max_digits=10, decimal_places=2,
        blank=False, null=False)
    rentabilidade_por_peca_base = models.PositiveIntegerField(
            'rendimento por peça-base',
            help_text='Atribua a quantidade de itens que este custo pode render.\nEx.: O custo do pacote de missangas rende 20 peças',
            null=False, blank=False)

    class Meta:
        verbose_name = "custo implicado à opção da variação relacionada a técnica artesanal"
        verbose_name_plural = "custos implicados à opção da variação relacionada a técnica artesanal"

    def __str__(self):
        return f"{self.tekoa_tecnica_variacao_valor} - {self.custo}"


class TempoTrabalhoConfig(models.Model):
    piso_categoria_artesa = models.DecimalField(
        'Média da categoria de artesã/ão',
        max_digits=10, decimal_places=2,
        blank=False, null=False)
    carga_horas_mes = models.DecimalField(
        'Carga horária por mês à considerar',
        max_digits=10, decimal_places=2,
        blank=False, null=False)
    data_atualizacao = models.DateField(
        'Data da atualização da média salarial', null=True, blank=True)
    observacoes = models.TextField(
        'observações',
        null=True, blank=True)

    class Meta:
        verbose_name = "Configuração do tempo-trabalho"
        verbose_name_plural = "4 - Configurações do tempo-trabalho"

    def __str__(self):
        return f"Média da categoria artesã/ão: R$ {self.piso_categoria_artesa} - Carga horária mensal: {self.carga_horas_mes} horas"


#############
## Cadastros
#############

from accounts.models import Usuaria

class ArtesanatoCadastro(models.Model):
    tekoa = models.ForeignKey(
            TekoaCadastro, on_delete=models.PROTECT,
            null=True, blank=True,
            related_name='artesanatos_cadastrados')
    tekoa_tecnica_artesanato = models.ForeignKey(
            TekoaTecnicaArtesanatoConfig, on_delete=models.PROTECT,
            null=False, blank=False,
            related_name='artesanatos_cadastrados')
    artesa = models.ForeignKey(
            Usuaria, on_delete=models.PROTECT,
            verbose_name='Artesã/ão',
            null=True, blank=True,
            related_name='artesanatos_cadastrados')
    data_registro = models.DateTimeField(
        'Data de registro', auto_now_add=True)
    #TODO Atualizar manual
    data_modificacao = models.DateTimeField(
        'Ultima modificação', null=True, blank=True)
    data_publicacao = models.DateTimeField(
        'Data de publicação', null=True, blank=True)

    def save(self, *args, **kwargs):

        if not self.id:
            super().save(*args, **kwargs)
            self.variacoes_init()

        super().save(*args, **kwargs)

    def variacoes_init(self):
        # Ler variacoes relacionadas a tecnica
        variacoes =\
            self.tekoa_tecnica_artesanato.variacoes_relacionadas.all()
        for item_variacao in variacoes:
            if item_variacao:
                ArtesanatoVariacao\
                    .objects.get_or_create(
                        artesanato=self,
                        variacao=item_variacao)

    def __str__(self):
        return f"{self.tekoa_tecnica_artesanato}"

    def get_absolute_url(self):
        return reverse('artesanato-detail', args=[str(self.id)])

    class Meta:
        ordering = ['-data_registro', '-id', 'tekoa_tecnica_artesanato', ]
        verbose_name = "Cadastro de artesanato"
        verbose_name_plural = "Cadastros de artesanato"

    def fator_rentabilidade_por_opcao_de_variacao(self, custo_artesanato, return_object=False):
        '''
          Verificar rendimento do material
          necessário para a tecnica artesanal
          aplicada à opção de variação.
          Este fator de rendimento será priorizado!
        '''
        variacoes_valores = [
            variacao.valor_variacao.id
            for variacao in self.variacoes.all()
            if variacao.valor_variacao
        ]
        # TODO checar se não forma unique_together
        obj_rentabilidade = TekoaTecnicaArtesanatoVariacaoValorRentabilidade\
                .objects.filter(
                    tekoa_tecnica_variacao_valor__in=variacoes_valores,
                    custo=custo_artesanato
                )\
                .select_related(
                    'custo',
                    'tekoa_tecnica_variacao_valor')\
                .order_by('rentabilidade_por_peca_base') # ordenação prioriza rentabilidade menor
        if obj_rentabilidade:
            if return_object:
                return obj_rentabilidade.first()
            else:
                return obj_rentabilidade.first().rentabilidade_por_peca_base
        else:
            return None

    def calculo_total_custos_artesanato(self):
        custos_artesanato=self.tekoa_tecnica_artesanato.custos_relacionados.all()
        soma_custos_artesanato=0
        for custo_artesanato in custos_artesanato:

            fator_rentabilidade_opcao_variacao =\
                self.fator_rentabilidade_por_opcao_de_variacao(custo_artesanato)
            if fator_rentabilidade_opcao_variacao:
                fator_rentabilidade=\
                    fator_rentabilidade_opcao_variacao
            else:
                # rendimento padrão: obtida do cadastro da técnica
                fator_rentabilidade=\
                    custo_artesanato.rentabilidade_por_peca_base

            if fator_rentabilidade:
                soma_custos_artesanato += custo_artesanato.valor / fator_rentabilidade
            else:
                soma_custos_artesanato += custo_artesanato.valor
        return soma_custos_artesanato

    def calculo_total_custos_variacoes(self):
        variacoes = self.variacoes.all()
        total_custo_variacoes = 0
        for variacao in variacoes:
            if not variacao.valor_variacao:
                continue

            soma_custo_variacao = 0
            for custo_variacao in variacao.valor_variacao.custos.all():
                if custo_variacao.rentabilidade_por_peca_base:
                    soma_custo_variacao += custo_variacao.valor / custo_variacao.rentabilidade_por_peca_base
                else:
                    soma_custo_variacao += custo_variacao.valor
            total_custo_variacoes += soma_custo_variacao
        return total_custo_variacoes

    def calculo_total_custos(self):
        return self.calculo_total_custos_artesanato() + self.calculo_total_custos_variacoes()

    def calculo_total_tempo_trabalho(self):
        tempo_trabalho = self.tekoa_tecnica_artesanato.tempo_trabalho_peca_base

        for variacao in self.variacoes.all():
            if not variacao.valor_variacao:
                continue

            if variacao.valor_variacao.tempo_trabalho_variacao:
                tempo_trabalho += variacao.valor_variacao.tempo_trabalho_variacao

        if not tempo_trabalho:
            # TODO checar o que fazer caso tempo_trabalho = None
            return 0

        return tempo_trabalho

    def calculo_valor_hora(self):
        tempo_trabalho_config = TempoTrabalhoConfig.objects.first()
        return tempo_trabalho_config.piso_categoria_artesa / tempo_trabalho_config.carga_horas_mes

    def calculo_total_valor_tempo_trabalho(self):
        return self.calculo_total_tempo_trabalho() * self.calculo_valor_hora()

    def calculo_total_valor_tempo_trabalho_round(self):
        valor = self.calculo_total_valor_tempo_trabalho()
        return round(valor, 2)

    def calculo_preco_justo(self):
        calculo = self.calculo_total_custos() + self.calculo_total_valor_tempo_trabalho()
        preco_justo = float(calculo)
        return round(preco_justo, 2)

    def calculo_preco_justo_desconto(self):
        # TODO parametrizar percent_desconto em calculo_preco_justo_desconto
        percent_desconto = 50
        preco_justo = self.calculo_preco_justo()
        preco_justo_desconto = (preco_justo * percent_desconto) / 100
        preco_justo_desconto = float(preco_justo_desconto)
        return round(preco_justo_desconto, 2)

    @property
    def foto_destaque(self):
        try:
            return self.fotos.get(destaque=True)
        except:
            return None

    """
    PUBLICAÇÂO DO ARTESANATO
    """

    def get_status_publicacao(self):
        # TODO checar se tem foto cadastrada
        # TODO checar se tem variação selecionada

        if not self.data_publicacao:
            return (
                'PUBLICAR',
                '<b>Este artesanato não está publicado!</b><br><br>Clique no botão "Publicar" para mostrar este item no catálogo. Além disso, uma mensagem de solicitação de publicação será enviada para o grupo de apoio.')

        # TODO Descontinuado 'ATUALIZAR_PUBLICACAO'.
        # if (self.data_modificacao
        #         and self.data_publicacao < self.data_modificacao):
        #     return (
        #         'ATUALIZAR_PUBLICACAO',
        #         '<b>A publicação deste artesanato precisa ser atualizada!</b><br><br>Este cadastro têm alterações não publicadas.<br>Clique no botão "Atualizar publicação" e uma mensagem de solicitação de atualização de publicação será enviada para o grupo de apoio.')
        else:
            return (
                'OK',
                '<b>Este artesanato já está publicado!</b>')

    def publicar_artesanato(self, mensagem='', host=''):
        # define mensagem de texto
        subject = 'Artesanato adicionado para publicação'
        message = 'O seguinte cadastro foi realizado para publicação: \n'

        # relaciona o link do artesanato ao texto
        link = reverse('publico-artesanato-detail', args=[str(self.id)])
        message += host
        message += link

        if mensagem:
            message += f'\n\nSeguido da mensagem da(o) artesã/ão {self.artesa}:'
            message += f'\n\n{ mensagem }'

        # envia o e-mail
        try:
            mail_managers(subject, message)
        except:
            raise

        self.data_publicacao = datetime.now()
        self.save()

        return True

    def atualizar_publicacao(self, mensagem='', host=''):
        # define mensagem de texto
        subject = 'Atualização de publicação de Artesanato'
        message = 'O seguinte cadastro foi atualizado para publicação: \n\n'

        # relaciona o link do artesanato ao texto
        link = reverse('publico-artesanato-detail', args=[str(self.id)])
        message += host
        message += link

        if mensagem:
            message += f'\n\nSeguido da mensagem da(o) artesã/ão {self.artesa}:'
            message += f'\n\n{ mensagem }'

        # envia o e-mail
        try:
            mail_managers(subject, message)
        except:
            raise

        self.data_publicacao = datetime.now()
        self.save()

        return True

    def retirar_publicacao(self, mensagem='', host=''):
        self.data_publicacao = None
        self.save()

        return True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._id = self.id

    def deletar_publicacao(self):

        """
        TODO Pensar melhor no delete de artesanato e publicação
        """


        if not self.data_publicacao:
            return True

        # define mensagem de texto
        subject = f'Deletar publicação de Artesanato {self._id}'
        message = f'O cadastro  {self._id} foi deletado \n'

        # envia o e-mail
        try:
            mail_managers(subject, message)
        except:
            raise

        return True

    def set_data_modificacao(self):
        self.data_modificacao = datetime.now()
        self.save()
        return True


@receiver(models.signals.post_delete, sender=ArtesanatoCadastro)
def auto_delete_artesanato_fotos_on_delete(sender, instance, **kwargs):
    for obj_foto in instance.fotos.all():
        if os.path.isfile(obj_foto.foto.path):
            os.remove(obj_foto.foto.path)


class ArtesanatoVariacao(models.Model):
    artesanato = models.ForeignKey(
            ArtesanatoCadastro, on_delete=models.CASCADE,
            null=False, blank=False,
            related_name='variacoes')
    variacao = models.ForeignKey(
            TekoaTecnicaArtesanatoVariacaoCadastro,
            verbose_name='variação', on_delete=models.SET_NULL,
            null=True, blank=True)
    valor_variacao = models.ForeignKey(
            TekoaTecnicaArtesanatoVariacaoValores,
            verbose_name='opção da variação', on_delete=models.SET_NULL,
            null=True, blank=True)

    def save(self, *args, **kwargs):
        # self.variacao = self.valor_variacao.tekoa_tecnica_variacao

        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.artesanato} - {self.variacao} {self.valor_variacao}"

    class Meta:
        ordering = ['-variacao__nome', 'valor_variacao__valor_variacao']


def artesanato_foto_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'artesanato/artesao_{0}/artesanato_{1}/{2}'.format(
        instance.artesanato.artesa.id,
        instance.artesanato.id,
        filename)

class ArtesanatoFoto(models.Model):
    artesanato = models.ForeignKey(
            ArtesanatoCadastro, on_delete=models.CASCADE,
            null=False, blank=False,
            related_name='fotos')
    """
    TODO:
    * Deletar arquivo caso seja alterado ou registro excluído;
    * Ter um limite máximo de peso do arquivo (1MB ?);
    *
    """
    # foto = models.ImageField(
    foto = ThumbnailerImageField(
        null=True, blank=True,
        upload_to=artesanato_foto_path,
        resize_source=dict(
            size=(800, 600),
            sharpen=False,
            crop="scale"
        )
    )
    destaque = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        artesanato_fotos = self.artesanato.fotos.all()
        if self.destaque:
            artesanato_fotos.update(destaque=False)

        if not artesanato_fotos:
            self.destaque = True

        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.destaque:
            artesanato = self.artesanato.fotos.exclude(id=self.id).first()
            if artesanato:
                artesanato.destaque=True
                artesanato.save()

        super().delete(*args, **kwargs)

    def __str__(self):
        return f"{self.foto}"

    class Meta:
        ordering = ['-destaque', 'id']


@receiver(models.signals.post_delete, sender=ArtesanatoFoto)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.foto:
        if os.path.isfile(instance.foto.path):
            os.remove(instance.foto.path)


##########################
## Consulta de resultados
##########################

def consulta_artesanato_foto_path(instance, filename):
    return 'consulta/{0}/{1}/{2}/{3}'.format(
        slugify(instance.tekoa.nome),
        slugify(instance.combinacao_variacao.first().tekoa_tecnica_variacao\
                .tekoa_tecnica_artesanato.tecnica_artesanato.nome),
        '-'.join([slugify(str(c.valor_variacao))
                  for c in instance.combinacao_variacao.order_by('id')]),
        filename)

class ConsultaArtesanatoFoto(models.Model):
    foto = ThumbnailerImageField(
        null=True, blank=True,
        upload_to=consulta_artesanato_foto_path,
        resize_source=dict(
            size=(800, 600),
            sharpen=False,
            crop="scale"
        )
    )
    combinacao_variacao = models.ManyToManyField(
            TekoaTecnicaArtesanatoVariacaoValores)
    tekoa = models.ForeignKey(
            TekoaCadastro, on_delete=models.PROTECT,
            null=True, blank=True,
            related_name='consulta_artesanato_fotos')
    user_add = models.ForeignKey(
            Usuaria, on_delete=models.PROTECT,
            verbose_name='Usuária que adicionou a foto',
            null=True, blank=True,
            related_name='consulta_artesanato_fotos_adicionadas')
    data_registro = models.DateTimeField(
        'Data de registro', auto_now_add=True)

    class Meta:
        ordering = ['-data_registro', ]


@receiver(models.signals.post_delete, sender=ConsultaArtesanatoFoto)
def consulta_artesanato_foto_auto_delete_file_on_delete(
        sender, instance, **kwargs):
    if instance.foto:
        if os.path.isfile(instance.foto.path):
            os.remove(instance.foto.path)


class ArtesanatosInteresse(models.Model):
    artesanatos = models.ManyToManyField(ArtesanatoCadastro)
    telefone = models.CharField('telefone / cel.', max_length=20,
                                null=True, blank=True,
                                help_text="Digite o DDD e o número do telefone.")
    email = models.EmailField('e-mail', null=True, blank=True)
    nome = models.CharField('nome', max_length=30, null=False, blank=False)
