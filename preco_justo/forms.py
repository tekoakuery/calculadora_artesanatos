from django import forms
from django.core.validators import EmailValidator, RegexValidator, MinLengthValidator
from dal import autocomplete
from . import models


class ArtesanatoCreateModelForm(forms.ModelForm):
    # def __init__(self,*args,**kwargs):
    #     super().__init__(*args,**kwargs)

    #     self.fields['tekoa_tecnica_artesanato'].queryset=\
    #         models.TecnicaArtesanatoCadastro.objects\
    #         .filter(tekoa_relacionadas=)

    #     return super().__init__(*args,**kwargs)

    class Meta:
        model = models.ArtesanatoCadastro
        fields = ['tekoa_tecnica_artesanato',]
        labels = {
            'tekoa_tecnica_artesanato': 'Técnica artesanal',
        }
        widgets = {
            'tekoa_tecnica_artesanato': autocomplete\
            .ModelSelect2(url='tecnica_artesanal-autocomplete')
        }


class ArtesanatoUpdateModelForm(forms.ModelForm):

    class Meta:
        model = models.ArtesanatoCadastro
        fields = ['tekoa_tecnica_artesanato',]
        labels = {
            'tekoa_tecnica_artesanato': 'Técnica artesanal',
        }
        widgets = {
            'tekoa_tecnica_artesanato': autocomplete\
            .ModelSelect2(url='tecnica_artesanal-autocomplete'),
        }

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)

    #     if self.instance.id:
    #         self.fields['tekoa_tecnica_artesanato'].initial=\
    #             self.instance.tekoa_tecnica_artesanato.id
    #         self.fields['tekoa_tecnica_artesanato'].choices = [(
    #             self.instance.tekoa_tecnica_artesanato.id,
    #             self.instance.tekoa_tecnica_artesanato.tecnica_artesanato.nome
    #         )]

    #     super().__init__(*args, **kwargs)


class ArtesanatoVariacaoModelForm(forms.ModelForm):

    class Meta:
        model = models.ArtesanatoVariacao
        fields = ['variacao', 'valor_variacao']
        # labels = {
        #     'variacao': ' ',
        #     'valor_variacao': 'Opção',
        # }
        # widgets = {
        #     'variacao': autocomplete\
        #     .ModelSelect2(url='variacao-autocomplete'),
        #     'valor_variacao': autocomplete\
        #     .ModelSelect2(url='variacao_valor-autocomplete',
        #                   forward=['variacao']),
        # }


ArtesanatoFormSet = forms.inlineformset_factory(
    models.ArtesanatoCadastro,
    models.ArtesanatoVariacao,
    form=ArtesanatoVariacaoModelForm,
    extra=0,
    can_delete=False,
)


class ArtesanatoFotoModelForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):

        if 'pk' in kwargs:
            artesanato_id = kwargs.pop('pk')
            kwargs.update(initial={
                'artesanato': artesanato_id
            })

        super().__init__(*args,**kwargs)

        return super().__init__(*args,**kwargs)

    class Meta:
        model = models.ArtesanatoFoto
        fields = ['artesanato', 'foto', 'destaque']
        widgets = {
            'artesanato': forms.HiddenInput()
        }
        labels = {
            'destaque': 'Definir como foto destaque',
        }


class ArtesanatoFavoritoContactForm(forms.ModelForm):
    class Meta:
        model = models.ArtesanatosInteresse
        fields = ['telefone', 'email', 'nome']

    def clean_telefone(self):
        data = self.data

        if not data['telefone'].strip() and not data['email'].strip():
            raise forms.ValidationError("Por favor, informe um telefone / cel. ou e-mail");
        return data['telefone']
