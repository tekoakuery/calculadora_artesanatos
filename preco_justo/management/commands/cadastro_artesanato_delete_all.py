from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from preco_justo.models import ArtesanatoCadastro


class Command(BaseCommand):

    def handle(self, *args, **options):

        print(ArtesanatoCadastro.objects.all().count())
        ArtesanatoCadastro.objects.all().delete()
        print(ArtesanatoCadastro.objects.all().count())
