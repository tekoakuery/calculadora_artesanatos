from django.contrib import admin

from accounts.models import Usuaria
from . import models


class TekoaTecnicaArtesanatoConfigInline(admin.TabularInline):
    model = models.TekoaTecnicaArtesanatoConfig
    can_delete = True
    extra=1
    ordering = ('tecnica_artesanato',)
    fields = ('tecnica_artesanato', )
    autocomplete_fields = ['tecnica_artesanato',]


class UsuariaInline(admin.TabularInline):
    model = Usuaria
    can_delete = True
    extra=1
    ordering = ('first_name',)
    fields = ('first_name', 'last_name')


@admin.register(models.TekoaCadastro)
class TekoaCadastroAdmin(admin.ModelAdmin):
    inlines = (TekoaTecnicaArtesanatoConfigInline,
            # UsuariaInline
            )
    search_fields = ['nome',]
    fields = (
        'nome', 'descricao', 'email',
        # 'access_token'
    )


@admin.register(models.TempoTrabalhoConfig)
class TempoTrabalhoConfigAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'data_atualizacao')


@admin.register(models.TecnicaArtesanatoCadastro)
class TecnicaArtesanatoCadastroAdmin(admin.ModelAdmin):
    search_fields = ['nome',]

    def get_model_perms(self, request):
        return {}


class TekoaTecnicaArtesanatoVariacaoCadastroInline(admin.TabularInline):
    model = models.TekoaTecnicaArtesanatoVariacaoCadastro
    can_delete = True
    extra=1
    ordering = ('nome',)
    fields = ('nome', 'descricao', 'opcional')


class TekoaTecnicaArtesanatoCustosInline(admin.TabularInline):
    model = models.TekoaTecnicaArtesanatoCustos
    can_delete = True
    extra=1
    ordering = ('custo',)
    fields = ('custo', 'descricao', 'valor', 'rentabilidade_por_peca_base')


@admin.register(models.TekoaTecnicaArtesanatoConfig)
class TekoaTecnicaArtesanatoConfigAdmin(admin.ModelAdmin):
    list_filter = ('tekoa', 'tecnica_artesanato')
    autocomplete_fields = ['tekoa', 'tecnica_artesanato']
    inlines = (TekoaTecnicaArtesanatoCustosInline,
            TekoaTecnicaArtesanatoVariacaoCadastroInline)
    list_display = ('tecnica_artesanato', 'tekoa')



# @admin.register(models.TekoaTecnicaArtesanatoCustos)
# class TekoaTecnicaArtesanatoCustosAdmin(admin.ModelAdmin):
#     # TODO filtro por tekoa e por técnica
#     pass


@admin.register(models.TekoaTecnicaArtesanatoVariacaoCadastro)
class TekoaTecnicaArtesanatoVariacaoCadastroAdmin(admin.ModelAdmin):
    search_fields = ['nome',]

    def get_model_perms(self, request):
        return {}


class TekoaTecnicaArtesanatoCustosInline(admin.TabularInline):
    model = models.TekoaTecnicaArtesanatoVariacaoValorCustos
    can_delete = True
    extra=1
    ordering = ('custo',)
    fields = ('custo', 'descricao', 'valor', 'rentabilidade_por_peca_base')


class TekoaTecnicaArtesanatoVariacaoValorRentabilidadeInline(admin.TabularInline):
    model = models.TekoaTecnicaArtesanatoVariacaoValorRentabilidade
    can_delete = True
    extra=1
    ordering = ('custo',)
    fields = ('custo', 'rentabilidade_por_peca_base',)


@admin.register(models.TekoaTecnicaArtesanatoVariacaoValores)
class TekoaTecnicaArtesanatoVariacaoValoresAdmin(admin.ModelAdmin):
    inlines = (TekoaTecnicaArtesanatoCustosInline,
            TekoaTecnicaArtesanatoVariacaoValorRentabilidadeInline)
    autocomplete_fields = ['tekoa_tecnica_variacao',]
    list_filter = (
        'tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tekoa',
        'tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tecnica_artesanato',
        'tekoa_tecnica_variacao__nome',
    )
    list_display = (
        # 'tekoa_tecnica_variacao__tekoa_tecnica_artesanato__tecnica_artesanato',
        'get_variacao',
        'get_tecnica_artesanal',
        'get_tekoa',
    )

    def get_variacao(self, obj):
        try:
            return \
                f"{obj.tekoa_tecnica_variacao.nome}: \
                {obj.valor_variacao}"
        except:
            return 'Not Available'
    get_variacao.short_description = 'Opção da Técnica artesanal'

    def get_tecnica_artesanal(self, obj):
        try:
            return \
                obj.tekoa_tecnica_variacao\
                .tekoa_tecnica_artesanato.tecnica_artesanato
        except:
            return 'Not Available'
    get_tecnica_artesanal.short_description = 'Técnica artesanal'

    def get_tekoa(self, obj):
        try:
            return \
                obj.tekoa_tecnica_variacao.tekoa_tecnica_artesanato.tekoa
        except:
            return 'Not Available'
    get_tekoa.short_description = 'Tekoa'


# @admin.register(models.TekoaTecnicaArtesanatoVariacaoValorCustos)
# class TekoaTecnicaArtesanatoVariacaoValorCustosAdmin(admin.ModelAdmin):
#     pass
