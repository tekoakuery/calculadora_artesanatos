# Generated by Django 3.2.16 on 2023-01-22 21:05

from django.db import migrations, models
import django.db.models.deletion
import preco_justo.models


class Migration(migrations.Migration):

    dependencies = [
        ('preco_justo', '0022_auto_20230104_0121'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='artesanatocadastro',
            options={'ordering': ['-data_registro', '-id', 'tekoa_tecnica_artesanato'], 'verbose_name': 'Cadastro de artesanato', 'verbose_name_plural': 'Cadastros de artesanato'},
        ),
        migrations.CreateModel(
            name='ArtesanatoFoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto', models.ImageField(blank=True, null=True, upload_to=preco_justo.models.artesanato_foto_path)),
                ('destaque', models.BooleanField(default=0)),
                ('artesanato', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='fotos', to='preco_justo.artesanatocadastro')),
            ],
            options={
                'ordering': ['-destaque', '-foto'],
            },
        ),
    ]
