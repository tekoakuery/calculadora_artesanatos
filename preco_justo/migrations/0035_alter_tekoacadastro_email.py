# Generated by Django 3.2.23 on 2024-03-02 03:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('preco_justo', '0034_auto_20240301_2357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tekoacadastro',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='e-mail de contato'),
        ),
    ]
