# Generated by Django 3.2.13 on 2022-06-20 22:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('preco_justo', '0015_auto_20220620_1945'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tekoatecnicaartesanatovariacaocadastro',
            name='opcional',
            field=models.BooleanField(help_text='Selecione este campo caso a variação não sirva todes artesãs/ãos', verbose_name='variação opcional'),
        ),
    ]
