# Generated by Django 3.2.23 on 2024-03-06 04:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('preco_justo', '0036_consultaartesanatofoto'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='consultaartesanatofoto',
            options={'ordering': ['-data_registro']},
        ),
    ]
