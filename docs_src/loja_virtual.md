# Loja Virtual
...

## Contexto
...

## Objetivo
...

## Manual de uso
...


### Configuração da Tekoa
...

#### Painel de configurações
...
[vídeo-tutorial](https://www.youtube.com/watch?v=jJKZ1rpnsjg)

#### Configuração
...
[vídeo-tutorial](https://www.youtube.com/watch?v=jJKZ1rpnsjg)

#### Configurar redes sociais
...
[vídeo-tutorial](https://www.youtube.com/watch?v=BPaiy4Vd508)

#### Configurar sistema de pagamento
...
[vídeo-tutorial](https://www.youtube.com/watch?v=KrLQ5RUuEhE)


### Processo de compra
...
[vídeo-tutorial](https://www.youtube.com/watch?v=z5CG0Ps1sX4)

### Aviso de novos pedidos
...
[vídeo-tutorial](https://www.youtube.com/watch?v=KrLQ5RUuEhE)

### Painel de pedidos
...
[vídeo-tutorial](https://www.youtube.com/watch?v=cPiEvsI7HD8)


### Painel de controle financeiro
...

#### Tutorial wirecard
...
[vídeo-tutorial](https://www.youtube.com/watch?v=ptU4vnCbnn4)

##### Pagamento: Teste prático
...
* Criar produto teste - [vídeo-tutorial](https://www.youtube.com/watch?v=h6WefwB2ErU)
* Verificação da venda - [vídeo-tutorial](https://www.youtube.com/watch?v=bKrokSTzgfA)


### Cadastro de produtos
...

#### Primeiros passos
...
[vídeo-tutorial](https://www.youtube.com/watch?v=yXdE0a8lobI)

#### Configurar variações
...
[vídeo-tutorial](https://www.youtube.com/watch?v=K9UQwb3GnqE)

##### Outros exemplos:
[vídeo-tutorial](https://www.youtube.com/watch?v=ycG3C3h0cSc)

#### Configurar dimensões
...
[vídeo-tutorial](https://www.youtube.com/watch?v=i2mPn9uhFPM)

#### Inserir imagens
...
[vídeo-tutorial](https://www.youtube.com/watch?v=I1X_htfep5o)

#### Cálculo de frete
...
[vídeo-tutorial](https://www.youtube.com/watch?v=xs9Y1ZQ9nEA)
