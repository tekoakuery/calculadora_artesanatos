# Calculadora Artesanatos / Instalação

## Requirements

* Python 3.7+
* Postgresql 12+

## Clone

```sh
git clone https://gitlab.com/tekoakuery/calculadora_artesanatos.git --recurse-submodules
cd calculadora_artesanatos
```

## Create venv

```sh
virtualenv -p python3.9 venv && source venv/bin/activate
```

## Install Python packages

```sh
pip install -r requirements.txt # or requirements/local.txt
```

## Configure application

* Copy configuration sample:
    ```sh
    cp calculadora_artesanatos/settings_local-sample.py calculadora_artesanatos/settings_local.py
    ```
* Edit file and config database and others.

## Migrate database

```sh
python manage.py migrate
```

## Running Django server (to dev)

```sh
python manage.py runserver
```
