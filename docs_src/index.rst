.. Tekoa Kuery documentation, created by
   sphinx-quickstart on Sun Jun  6 21:41:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tekoa Kuery
===========

Valorização cultural através dos trabalhos artesanais das comunidades Guarani do litoral paulista.

.. toctree::
   :maxdepth: 1
   :caption: Sumário:

   Resumo <tekoakuery_resumo.md>
   Calculadora do Preço Justo <README.rst>
   Loja virtual <loja_virtual.md>
   Publicação de conteúdos <publicacao_conteudos.md>


Índices e tabelas
==================

* :ref:`genindex`
* :ref:`modindex`
