# Django LTS
# https://www.djangoproject.com/download/
Django<4

# Models
django-model-utils

# DB
postgres
psycopg2

# Forms
django-braces
django-crispy-forms

# As of django-crispy-forms 2.0 the template packs are now in separate packages
# https://stackoverflow.com/questions/75495403/django-returns-templatedoesnotexist-when-using-crispy-forms
# crispy-bootstrap4
crispy-bootstrap5

# Lib excel to import data
pyexcel-xlsx

# Autocomplete
django-autocomplete-light

# Lib to image
pillow

# automatically deletes files for FileField, ImageField
django-cleanup

# upload chunked and ajax interface
django-fine-uploader

# thumbnailing application
easy-thumbnails
